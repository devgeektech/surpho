var mongoose    = require('mongoose');
var models      = require('mongoose').models;


var PhoneDirectory = mongoose.Schema({
    name: { type: String, trim: true },
    phone: String,
    userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
});

mongoose.model('PhoneDirectory', PhoneDirectory);