var mongoose = require('mongoose');
var models = require('mongoose').models;
var bcrypt = require('bcrypt-nodejs');
var _ = require('lodash');


var wordSchema = mongoose.Schema({
    actual_word: { type: String, default: '', trim: true },
    manipulate_word: { type: String, default: '', trim: true },
    status: { type: Boolean, default: true },
    isBlocked: { type: Boolean, default: false },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
});
mongoose.model('Word', wordSchema);