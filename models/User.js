var mongoose = require('mongoose');
var models = require('mongoose').models;
var bcrypt = require('bcrypt-nodejs');
var _ = require('lodash');
const async = require('async');


var User = mongoose.Schema({
    name: { type: String, default: '', trim: true },
    userName: { type: String, default: '', trim: true},
    phoneNumber: {
        countryCode: { type: String, default: '' },
        phone: { type: String, default: '' }
    },
    countryCodeAndNumber: { type: String, default: ''},
    isPhoneVerified: { type: Boolean, default: false },
    profilePic: { type: String, default: '', trim: true },
    language: { type: String },
    email: { type: String, default: '', trim: true },
    dob: { type: String, default: '', trim: true },
    password: { type: String, default: '' },
    description: { type: String, default: '', trim: true },
    token: { type: String, default: '' },
    role: { type: String, default: 'user', enum: ['admin', 'user'] },
    status: { type: Boolean, default: true },
    isBlockedByAdmin: { type: Boolean, default: false },
    isRegistration: { type: Boolean, default: false },
     temporaryToken:{ type: String, default: '' },
    online: { type: Boolean, default: false },
    views: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    viewCounter: { type: Number, default: 0 },
    chatId: { type: String, default: '', trim: true },
    followers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    following: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    friends: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    recentUsers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    social_media_links: {
        facebook: {
            id: { type: String, default: '', trim: true },
            token: { type: String, default: '', trim: true },
            displayName: { type: String, default: '', trim: true },
            username: { type: String, default: '', trim: true }
        },
        instagram: {
            id: { type: String, default: '', trim: true },
            token: { type: String, default: '', trim: true },
            displayName: { type: String, default: '', trim: true },
            username: { type: String, default: '', trim: true }
        },
        twitter: {
            id: { type: String, default: '', trim: true },
            token: { type: String, default: '', trim: true },
            displayName: { type: String, default: '', trim: true },
            username: { type: String, default: '', trim: true }
        },
        google: {
            id: { type: String, default: '', trim: true },
            token: { type: String, default: '', trim: true },
            displayName: { type: String, default: '', trim: true },
            username: { type: String, default: '', trim: true }
        }
    },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    device: {
        deviceId: { type: String, default: '', trim: true },
        deviceType: { type: String, default: 'android', enum: ['web', 'android', 'ios'] }
    },
    location: {
        name: { type: String, default: '', trim: true },
        point: { type: [Number], index: { type: '2dsphere', sparse: true } }
    },
    currentLocation: {
        currentLocationName: { type: String, default: '', trim: true },
        currentLocationPoint: { type: [Number], index: { type: '2dsphere', sparse: true } }
    },
    isPrivate: { type: Boolean, default: false},
    firebaseId: { type: String, default: null},
    blockedBy: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    blockedUsers: [{ type: mongoose.Schema.Types.ObjectId, ref: "User"}],
    pendingRequests: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    rejectedRequests: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    seen:{type:Boolean,default:true}
});


User.pre('save', function(next) {
    this.updated_at = Date.now();
    next();
});


// methods ======================
// generating a hash
User.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
User.methods.validPassword = function(password) {
    if ( ! this.password ) { return false; }
    return bcrypt.compareSync( password, this.password );
};
// checking if password is valid
// User.methods.validPassword = function(password) {
//     console.log(password, this.password, 'testing');
//     return bcrypt.compareSync(password, this.password);
// };


User.methods.otherFollowing = function (user, id) {
    console.log("user wala datacagh", user._id);
    
    var addFollowing = 0;
        for(i=0; i < id.length; i++) {
            if (user._id.generationTime  == id[i].generationTime ) {
                addFollowing = 1
            }
        }
    return !(!addFollowing);
};

User.methods.otherFollowers = function (user, id) {
    console.log("user wala datacagh", user._id);
    var addFollower = 0;
        for(i=0; i < id.length; i++) {
            if (user._id.generationTime  == id[i].generationTime ) {
                addFollower = 1
            }
        }
    return !(!addFollower);
};

User.methods.addFollowingFlag = function (user, id) {
    console.log("user wala datacagh", user.following);
    var addFollowing = 0;
    _.each(user._doc.following, function(item, index) {
        if (item.toString() == id) {
            addFollowing = 1
        }
    });
    return !(!addFollowing);
};

User.methods.addFollowingFlagSearch = function (user, id) {
    console.log("user wala datacagh", user.following);
    var addFollowing = 0;
    _.each(id._doc.following, function(item, index) {
        if (item.toString() == user._id) {
            addFollowing = 1
        }
    });
    return !(!addFollowing);
};

User.methods.addFollowersFlagSearch = function(user, id) {
    var addFollower = 0;
    _.each(id._doc.followers, function(item, index) {
        //console.log(item.toString());
        if (item.toString() == user._id) {
            addFollower = 1
        }
    });
    return !(!addFollower);
};

User.methods.addFollowersFlag = function(user, id) {
    var addFollower = 0;
    _.each(user._doc.followers, function(item, index) {
        //console.log(item.toString());
        if (item.toString() == id) {
            addFollower = 1
        }
    });
    return !(!addFollower);
};


User.methods.addFriendsFlag = function(user, id) {
    var addFriend = 0
    _.each(user._doc.friends, function(item, index) {
        if (item.toString() == id) {
            addFriend = 1
        } 
    });
    return !(!addFriend);
};

User.methods.addFriendsFlagSearch = function(user, id) {
    var addFriend = 0
    _.each(id._doc.friends, function(item, index) {
        if (item.toString() == user._id) {
            addFriend = 1
        } 
    });
    return !(!addFriend);
};

User.methods.addrequestFlagSearch = function(user, id) {
    var addRequests;
    _.each(user._doc.pendingRequests, function(item, index) {
        console.log("item wala data",item);
        if (item.generationTime  == id._id.generationTime) {
           addRequests = 1;
        }else{
           addRequests = 0;
        }
    });
    return !(!addRequests);
};



User.methods.addCounts = function(user, deactivatedUsers) {
    _.each(user._doc.blockedUsers, function(item) { 
        var index = user._doc.following.indexOf(item);
        if (index > -1) {
            user._doc.following.splice(index, 1);
        }
        var index = user._doc.friends.indexOf(item);
        if (index > -1) {
            user._doc.friends.splice(index, 1);
        }
        var index = user._doc.followers.indexOf(item);
        if (index > -1) {
            user._doc.followers.splice(index, 1);
        }
        var index = user._doc.views.indexOf(item);
        if (index > -1) {
            user._doc.views.splice(index, 1);
        }
    });
    if(deactivatedUsers) {
        _.each(deactivatedUsers, function(item) { 
            var index = user._doc.following.indexOf(item._id);
            if (index > -1) {
                user._doc.following.splice(index, 1);
            }
            var index = user._doc.friends.indexOf(item._id);
            if (index > -1) {
                user._doc.friends.splice(index, 1);
            }
            var index = user._doc.followers.indexOf(item._id);
            if (index > -1) {
                user._doc.followers.splice(index, 1);
            }
            var index = user._doc.views.indexOf(item._id);
            if (index > -1) {
                user._doc.views.splice(index, 1);
            }
        });
    }
    return {
        followingCount: user._doc.following? user._doc.following.length:0,
        friendsCount: user._doc.friends? user._doc.friends.length:0 ,
        followersCount: user._doc.followers? user._doc.followers.length: 0,
        viewsCount: user._doc.views? user._doc.views.length : 0,
        blockedUsersCount: user._doc.blockedUsers? user._doc.blockedUsers.length : 0
    };
};


User.methods.addBlockedUserFlag = function(user, id) {
    var blockedUsers = 0;
    _.each(user._doc.blockedUsers, function(item, index) {
        if (item.toString() == id) {
            blockedUsers = 1
        } 
    });
    return !(!blockedUsers);
};


User.methods.addFollowRequestFlag = function(user, id) {
    // console.log("user", user._id);
    var addRequests = 0;
    _.each(user.pendingRequests, function(item, index) {
        console.log("item wala data",item);
        if (item == id) {
            addRequests = 1
        }
    });
    return !(!addRequests);
};

User.virtual('concatPhoneAndCountryCode').get(function(){
    return this.phoneNumber.countryCode + this.phoneNumber.phone;
})

mongoose.model('User', User);
