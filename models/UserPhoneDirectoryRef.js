var mongoose    = require('mongoose');
var models      = require('mongoose').models;


var UserPhoneDirectoryRef = mongoose.Schema({
    userAndNumberId: {type: mongoose.Schema.Types.ObjectId, ref: 'PhoneDirectory'},
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
});

mongoose.model('UserPhoneDirectoryRef', UserPhoneDirectoryRef);