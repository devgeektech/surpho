var mongoose = require('mongoose');
var models = require('mongoose').models;
var bcrypt = require('bcrypt-nodejs');
var _ = require('lodash');


var Notification = mongoose.Schema({
    title: { type: String, default: '', trim: true },
    description: { type: String, default: '', trim: true },
    type: { type: String, default: 'likePost', enum: ['likePost', 'blockUser', 'followRequest', 'commentOnPost', 'tagOnPost', "follow", "friends"] },
    status: { type: Boolean, default: true },
    to: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    from: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    post: { type: mongoose.Schema.Types.ObjectId, ref: 'Post' },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    isRead: { type: Boolean, default: false,  ref: 'User' }
});


Notification.pre('save', function (next) {
    this.updated_at = Date.now();
    next();
});


mongoose.model('Notification', Notification);