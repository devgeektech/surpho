var mongoose = require('mongoose');
var models = require('mongoose').models;
var bcrypt = require('bcrypt-nodejs');
var _ = require('lodash');


var ChatRoom = mongoose.Schema({
    name: { type: String, default: '', trim: true },
    members: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    messages: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Message' }],
    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() }
});


ChatRoom.pre('save', function(next) {
    this.updated_at = Date.now();
    next();
});



mongoose.model('ChatRoom', ChatRoom);