var mongoose = require('mongoose');
var models = require('mongoose').models;
var bcrypt = require('bcrypt-nodejs');
var _ = require('lodash');


var Post = mongoose.Schema({
    title: { type: String, default: '', trim: true },
    description: { type: String, default: '', trim: true },
    status: { type: Boolean, default: true },
    isBlocked: { type: Boolean, default: false },
    media: [],
    tags:[{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }],
    viewsTrending:[{
        viewTime: {type: Date, default: Date.now},
        viewedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    }],
    viewCounter: {type: Number, default: 0},
    views: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    likes: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    reports: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    isTrending: { type: Boolean, default: false}
});


Post.pre('save', function(next) {
    this.updated_at = Date.now();
    next();
});

Post.methods.addViews = function(user, myId) {
    var addFollower;
    _.each(user._doc.followers, function(item, index) {
        if (item.toString() == myId) {
            addFollower = 1
        } else addFollower = 0
    });
    return !(!addFollower);
};


Post.methods.addlike = function(post, myId) {
    var addLike;
    _.each(post._doc.likes, function(item, index) {
        if(!addLike) {
            if (item.toString() == myId) {
                addLike = 1
                
            } else addLike = 0
        };
    });
    return !(!addLike);
};

Post.methods.counts = function(post) {
    return {
        likesCount: post._doc.likes.length,
        viewsCount: post._doc.views.length,
        commentsCount: post._doc.comments.length
    }
};

mongoose.model('Post', Post);
