var mongoose = require('mongoose');
var models = require('mongoose').models;
var bcrypt = require('bcrypt-nodejs');
var _ = require('lodash');


var Message = mongoose.Schema({
    media: { type: String, default: '', trim: true },
    text: { type: String, default: '', trim: true },
    from: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    to: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() }
});


Message.pre('save', function(next) {
    this.updated_at = Date.now();
    next();
});



mongoose.model('Message', Message);