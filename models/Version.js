var mongoose = require('mongoose');
var models = require('mongoose').models;
var bcrypt = require('bcrypt-nodejs');
var _ = require('lodash');


var Version = mongoose.Schema({
    normalAndroidVersion: { type: String, default: '', trim: true },
    criticalAndroidVersion: { type: String, default: '', trim: true },
    normalIOSVersion: { type: String, default: '', trim: true },
    criticalIOSVersion: { type: String, default: '', trim: true },
    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() }
});


Version.pre('save', function(next) {
    this.updated_at = Date.now();
    next();
});



mongoose.model('Version', Version);