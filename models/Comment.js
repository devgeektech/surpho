var mongoose = require('mongoose');
var models = require('mongoose').models;
var bcrypt = require('bcrypt-nodejs');
var _ = require('lodash');


var Comment = mongoose.Schema({
    text: { type: String, default: '', trim: true },
    status: { type: Boolean, default: true },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
});


Comment.pre('save', function(next) {
    this.updated_at = Date.now();
    next();
});


mongoose.model('Comment', Comment);