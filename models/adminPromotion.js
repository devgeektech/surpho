var mongoose = require('mongoose');
var models = require('mongoose').models;
var bcrypt = require('bcrypt-nodejs');
var _ = require('lodash');


var PromotionSchema = mongoose.Schema({
    promotionText: { type: String, default: '', trim: true },
    image:[{'image_file':{type: String}}],
    video:[{'video_file':{type: String}}],
    status: { type: Boolean, default: true },
    isBlocked: { type: Boolean, default: false },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    promoLocation: { 'type': { type: String, enum: "Point", default: "Point" }, coordinates: { type: [Number], default: [0, 0] } },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
});
PromotionSchema.index({promoLocation:'2dsphere'});
mongoose.model('Promotion', PromotionSchema);