'use strict';
var models = require('mongoose').models;
module.exports = function(res) {
    return {
        success: function(message, code) {
            res.status(code).send({
                is_success: true,
                responseCode: code,
                message: message || error,
                data: {}
            });
        },
        failure: function(error, code, message) {
            res.status(code).send({
                is_success: false,
                responseCode: code,
                message: message || error,
                data: {}
            });
        },
        data: function(item, code) {
            res.status(code).send({
                is_success: true,
                responseCode: code,
                data: item
            });
        },
        page: function(items, code, total, skiped) {
            res.status(code).send({
                is_success: true,
                responseCode: code,
                data: {
                    items: items,
                    skiped: skiped || 0,
                    total: total || items.length
                }
            });
        }
    };
};