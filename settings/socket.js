const jwt = require('jsonwebtoken');
var fs = require('fs');
var join = require('path').join;
var config = require('../settings/config');
var redis = require("redis");
var client = require('redis').createClient();
var subscribe = require('redis').createClient();
const models = require('mongoose').models;
var androidPush = require('../components/androidNotification');
const iosPush = require('../components/iosNotification');
const async = require('async');
const _ = require('lodash');
var roms;
var onlineStatusUpdate = function (decodedUser, socket, status, callback) {
    if (decodedUser) {
        models.User.findOneAndUpdate({ _id: decodedUser.id }, {
            online: status,
            chatId: socket.id
        }, function (err, user) {
            if (err) return callback(err); ``
            if (user) return callback(null, user._doc);
            else return callback(null, null);
        });
    }
};



var userDecoder = function (token, callback) {
    jwt.verify(token, config.auth.secret, {
        ignoreExpiration: true
    }, function (err, claims) {
        if (err) {
            return callback(err);
        } else {
            return callback(null, claims);
        }
    });
};

var saveRecentUser = function (myId, otherId, callback) {
    async.waterfall([
        function (cb) {
            models.User.findOne({ _id: myId })
                .exec(function (err, user) {
                    var ifExist = user.recentUsers.filter(function (item, index) {
                        return item.toString() == otherId;
                    });
                    if (!ifExist.length) {
                        user.recentUsers.unshift(otherId);
                    } else {
                        var deletedElement = _.remove(user.recentUsers, function (arrElement) {
                            return arrElement == otherId;
                        });
                        if (deletedElement) {
                            user.recentUsers.unshift(otherId);
                        }
                    }
                    user.save(function (err, recentUser) {
                        if (err) return cb(err);
                        cb(null, recentUser)
                    });

                })
        },
        function (usr, cb) {
            models.User.findOne({ _id: otherId })
                .exec(function (err, user) {
                    if (err) return cb(err);
                    var ifExist = user.recentUsers.filter(function (item, index) {
                        return item.toString() == myId
                    });
                    if (!ifExist.length) {
                        user.recentUsers.unshift(myId);
                    } else {
                        var deletedElement = _.remove(user.recentUsers, function (arrElement) {
                            return arrElement == myId;
                        });
                        if (deletedElement) {
                            user.recentUsers.unshift(myId);
                        }
                    }
                    user.save(function (err, recentUser) {
                        if (err) return cb(err);
                        cb(null, recentUser)
                    });
                })
        }
    ], function (err, user) {
        if (err) {
            return callback(err);
        }
        callback(null, user);
    })
}

var findRoom = function (myId, otherId, callback) {
    models.ChatRoom.findOne({ $or: [{ name: myId + '_' + otherId }, { name: otherId + '_' + myId }] }, function (err, ChatRoom) {
        callback(ChatRoom)
    })

}


var sendNotification = (userId, Id, message) => {
    models.User.findOne({
        '_id': userId
    })
        .exec(function (err, DataOther) {
            models.User.findOne({
                '_id': Id
            })
                .exec(function (err, userData) {
                    var data = {};
                    data.message = message;
                    data.title = 'chat notification';
                    data.type = 'chatNotification';
                    data.actionId = userData._id;
                    data.name = userData.name;
                    if (userData.device.deviceType == "android" && DataOther.device.deviceType == "android") {
                        androidPush.androidPushNotification(DataOther.device.deviceId, data, (err) => {

                        })
                    } else if (userData.device.deviceType == "ios" && DataOther.device.deviceType == "ios") {
                        iosPush.apn(DataOther.device.deviceId, data, (err) => {
                        })

                    } else if (userData.device.deviceType == "android" && DataOther.device.deviceType == "ios") {
                        iosPush.apn(DataOther.device.deviceId, data, (err) => {
                        })

                    } else if (userData.device.deviceType == "ios" && DataOther.device.deviceType == "android") {

                        androidPush.androidPushNotification(DataOther.device.deviceId, data, (err) => {
                        })

                    } else {
                        console.log("some thing wrong");
                    }
                })
        })
}


module.exports.configure = function (io) {

    io.on('connection', function (socket) {

        if (socket.id) {
            io.emit('connection', 'connected')

            userDecoder(socket.handshake.query.token, function (err, decodedUser) {
                if (decodedUser) {
                    onlineStatusUpdate(decodedUser, socket, true, function (err, user) {
                        socket.userId = decodedUser.id;
                        socket.name = decodedUser.name;
                        socket.on('createRoom', function (otherId) {
                            var myId = socket.userId;
                          
                            findRoom(myId, otherId, function (ChatRoom) {
                                if (ChatRoom) {
                                    socket.emit('createRoom', { success: true, name: ChatRoom.name });

                                } else {
                                    var room = new models.ChatRoom({

                                        name: myId + '_' + otherId,
                                        members: [myId, otherId]
                                    });
                                    room.save(function (err, newChatroom) {
                                        socket.emit('createRoom', { success: true, name: newChatroom.name });
                                    });
                                };
                            });
                        });



                        socket.on('send', function (options) {
                            if (options.message) {
                                saveRecentUser(socket.userId, options.otherId, function (user) {
                                });
                            }

                            findRoom(socket.userId, options.otherId, function (ChatRoom) {
                                if (ChatRoom) {
                                    if (options) {
                                        if (options.message) {
                                            options.timeStamp = Date.now();
                                            client.get(ChatRoom.name, function (err, data) {
                                                if (!data) {
                                                    var tempMessageArray = [];
                                                    tempMessageArray.push(options);
                                                    var stringArray = JSON.stringify(tempMessageArray);
                                                    client.set(ChatRoom.name, stringArray);
                                                } else {
                                                    var tempMessageArray = JSON.parse(data);
                                                    tempMessageArray.push(options);
                                                    var stringArray = JSON.stringify(tempMessageArray);
                                                    client.set(ChatRoom.name, stringArray);
                                                }
                                                socket.socketName = ChatRoom.name;
                                                var realTimeUserIdArray = [];
                                                for (var i in io.sockets.clients().connected) {
                                                    realTimeUserIdArray.push(io.sockets.clients().connected[i].userId);
                                                }
                                                if (realTimeUserIdArray.indexOf(options.otherId) === -1) {
                                                    sendNotification(options.otherId, socket.userId, options.message);
                                                }
                                                client.publish(ChatRoom.name, JSON.stringify(options));
                                            })
                                        };
                                    }

                                } else {
                                    var myId = socket.userId;
                                    options.timeStamp = Date.now();
                                    findRoom(myId, options.otherId, function (ChatRoom) {
                                        if (ChatRoom) {
                                            socket.emit('createRoom', { success: true, name: ChatRoom.name });
                                        } else {
                                            var room = new models.ChatRoom({
                                                name: myId + '_' + options.otherId,
                                                members: [myId, options.otherId]
                                            });
                                            room.save(function (err, newChatroom) {
                                                if (options) {
                                                    if (options.message) {
                                                        client.get(newChatroom.name, function (err, data) {
                                                            if (!data) {
                                                                var tempMessageArray = [];
                                                                tempMessageArray.push(options);
                                                                var stringArray = JSON.stringify(tempMessageArray);
                                                                client.set(newChatroom.name, stringArray);
                                                            } else {

                                                                var tempMessageArray = JSON.parse(data);
                                                                tempMessageArray.push(options);
                                                                var stringArray = JSON.stringify(tempMessageArray);
                                                                client.set(newChatroom.name, stringArray);

                                                            };
                                                            socket.socketName = newChatroom.name;
                                                            var realTimeUserIdArray = [];
                                                            for (var i in io.sockets.clients().connected) {
                                                                realTimeUserIdArray.push(io.sockets.clients().connected[i].userId);
                                                            }
                                                            if (realTimeUserIdArray.indexOf(options.otherId) === 1) {
                                                                sendNotification(options.otherId, options.message);
                                                            }
                                                            client.publish(newChatroom.name, JSON.stringify(options));
                                                        })
                                                    };
                                                };
                                            });
                                        };
                                    });
                                };
                            })

                        });

                        var callback = function (channel, data) {
                  

                            var parsedData = JSON.parse(data);
                            client.get(socket.userId, function (err, data) {
                                var lastmessage = JSON.parse(data);
                                if (lastmessage) {
                                    if (
                                        lastmessage.message != parsedData.message
                                    ) {


                                        client.set(socket.userId, JSON.stringify(parsedData));
                                        io.to(channel).emit('pingMessage', parsedData);
                                    } else {
                                        console.log('sdfgbdf')
                                    }
                                } else {
                                    client.set(socket.userId, JSON.stringify(parsedData));
                                    io.to(channel).emit('pingMessage', parsedData);
                                }

                            });
                        };


                        socket.on('getmessages', function (data) {

                            findRoom(socket.userId, data.otherId, function (ChatRoom) {
                                if (ChatRoom) {
                                    subscribe.subscribe(ChatRoom.name);

                                    socket.join(ChatRoom.name);


                                    client.get(ChatRoom.name, function (err, data) {
                                        socket.emit('conversations', JSON.parse(data));
                                        socket.emit('channelId', ChatRoom);
                                    });
                                };
                            });
                        });
                        socket.on('event', function (data) {
                            socket.disconnect();
                            this.emit('event', 2);
                        });



                        socket.on('disconnect', function () {

                            subscribe.unsubscribe("5ae0097146c894541afbb380_5ad989718940da131e42b1bd");
                            socket.leave(socket.id);
                            socket.leave(roms);
                            io.emit('disconnect', 'disconnected')
                            onlineStatusUpdate(decodedUser, socket, false, function (err, user) {

                            });
                        });
                    });
                } else {
                    socket.emit('userNotExist', { success: false, 'message': 'user not exist' })
                }
            });
        }


    });

    var callback = function (channel, data) {
        var parsedData = JSON.parse(data);
        io.to(channel).emit('pingMessage', parsedData);
    }
    subscribe.on('message', callback);
};