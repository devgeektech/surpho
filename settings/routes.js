const models = require('mongoose').models;
const auth = require('../middleware/authentication');
const user_controller = require('../controllers/users');
const admin_user_controller = require('../controllers/adminUsers');
const version_controller = require('../controllers/versions');
const upload_controller = require('../controllers/upload');
const post_controller = require('../controllers/posts');
const comment_controller = require('../controllers/comments');
const notification = require('../components/notificationLogs');
const promotion_controller = require('../controllers/adminPromotions');
const word_controller = require('../controllers/adminWords');
var express = require('express');
const config = require('../settings/config');
var mime = require('mime'); 
var app = express();
const path = require('path')
const fs = require('fs')
var multer  = require('multer');
var storage = multer.diskStorage({
    destination: function(req, file, callback) {
		callback(null, './uploads')
	},
    filename: function(req, file, callback) {
        var fileName = path.parse(file.originalname).name;
		callback(null, fileName + '-' + Date.now() + path.extname(file.originalname))
	}
})
var upload = multer({storage: storage});
module.exports.configure = function(app) {
    app.get('/', function(req, res) {
        res.render('index');
    });
    app.get('/privacyPolicy', function(req, res) {
        res.render('privacyPolicy');
    });
    app.get('/help', function(req, res) {
        res.render('help');
    });


    /*=====================user==============================*/

    app.post('/api/socialLogin', user_controller.social)
    app.post('/api/user/signUp', upload.any(), user_controller.signUp)
    app.post('/api/user/changePassword', auth.requiresToken, user_controller.changePassword)
    app.post('/api/user/forgotPassword', user_controller.forgotPassword)
    app.post('/api/user/login', user_controller.userLogin)
    app.post('/api/user/tokenVerification', user_controller.tokenVerification)
    app.get('/api/users', auth.requiresToken, user_controller.getList)
    app.get('/api/recent-users', auth.requiresToken, user_controller.getRecentUserList)
    app.get('/api/users/:id/otherUserFollower', auth.requiresToken, user_controller.otherUserFollowerList)
    app.put('/api/users', auth.requiresToken, user_controller.updateUser)
    app.put('/api/users/:id/follow', auth.requiresToken, user_controller.follow)
    app.put('/api/users/:id/unfollow', auth.requiresToken, user_controller.unfollow)
    app.get('/api/users/:id', auth.requiresToken, user_controller.getProfile)
    app.delete('/api/users', auth.requiresToken, user_controller.delete)
    app.post('/api/users/refreshToken', auth.requiresToken, user_controller.refreshToken);
    app.post('/api/users/checkPhoneExistence', auth.requiresToken, user_controller.checkPhoneExistence);
    app.post('/api/users/updatePhone', auth.requiresToken, user_controller.updateUserPhone)
    app.post('/api/users/getFollowUsers', auth.requiresToken, user_controller.getFollowUsers)
    app.post('/api/users/blockUser/:id', auth.requiresToken, user_controller.blockUser);
    app.post('/api/users/accountsettings', auth.requiresToken, user_controller.accountSettings);
    app.post('/api/users/respondToFollowRequest/:id', auth.requiresToken, user_controller.respondToFollowRequest);
    app.post('/api/users/uploadPhoneDirectory', auth.requiresToken, user_controller.uploadPhoneDirectory);
    app.post('/api/users/logOutUser', auth.requiresToken, user_controller.logOutUser);
    app.post('/api/user/promotions', user_controller.userPromotions);
    /*============================= admin user ======================================*/
    app.get('/api/users/profile/:id', auth.requiresToken, admin_user_controller.view_profile)
    app.get('/api/user/search/', auth.requiresToken, admin_user_controller.search)
    app.get('/api/user/list', auth.requiresToken, admin_user_controller.allUsers)
    app.post('/api/user/change-user-status', auth.requiresToken, admin_user_controller.change_user_status)
    app.post('/api/user/block-and-unblock', auth.requiresToken, admin_user_controller.block_and_unblock)
    app.get('/api/contact/list/:id', auth.requiresToken, admin_user_controller.getUserContact)
    app.post('/api/login', admin_user_controller.adminLogin)
    app.post('/api/register', admin_user_controller.signUp)

   /*===================== Promotions ===============================*/

    app.post('/api/promotion/create', auth.requiresToken, promotion_controller.create)
    app.get('/api/promotion/list', auth.requiresToken, promotion_controller.all)
    app.post('/api/promotion', auth.requiresToken,promotion_controller.update)
    app.delete('/api/promotion/:id', auth.requiresToken, promotion_controller.delete)
    app.get('/api/promotion/search', auth.requiresToken, promotion_controller.promotion_search)
    app.get('/api/promotion/:id', auth.requiresToken, promotion_controller.getPromotion)
    app.post('/api/promotion/block-and-unblock', auth.requiresToken, promotion_controller.block_and_unblock)
    app.post('/api/promotion/change-status', auth.requiresToken, promotion_controller.change_promotion_status)
   
    //*==========================word api ====================================================*/
    
    app.post('/api/word/create', auth.requiresToken, word_controller.create)
    app.get('/api/word/list', auth.requiresToken, word_controller.all)
    app.put('/api/word/:id', auth.requiresToken, word_controller.update)
    app.delete('/api/word/:id', auth.requiresToken, word_controller.delete)
    app.get('/api/word-search', auth.requiresToken, word_controller.word_search)
    app.get('/api/word/:id', auth.requiresToken, word_controller.get)
    app.post('/api/word/block-and-unblock', auth.requiresToken, word_controller.blockAndUnblockWord)
    app.post('/api/word/change-status', auth.requiresToken, word_controller.change_status)
    app.post('/api/word/abused-word', auth.requiresToken, word_controller.getAbusedWord)
 

    /*=====================comment===============================*/

  
  
  
  
  
  
    /*=====================version===========================*/

    app.post('/api/versions', version_controller.create)
    app.get('/api/versions', version_controller.all)
    app.get('/api/versions/:id', version_controller.get)
    app.put('/api/versions', version_controller.update)




    /*=====================upload============================*/
    app.post('/api/uploadMedia', auth.requiresToken, upload.array('media'), upload_controller.upload)




    /*=====================post===============================*/

    app.post('/api/posts', auth.requiresToken, post_controller.create)
    app.get('/api/posts', auth.requiresToken, post_controller.all)
    app.post('/api/post/change-status', auth.requiresToken, post_controller.change_status)
    app.post('/api/post/block-and-unblock', auth.requiresToken, post_controller.block_and_unblock)
    app.get('/api/posts/list', auth.requiresToken, post_controller.postList)
    app.get('/api/post/search', auth.requiresToken, post_controller.post_search)

    app.get('/api/posts/:id', auth.requiresToken, post_controller.get)
    app.put('/api/posts/:id', auth.requiresToken, post_controller.update)
    app.put('/api/posts/:id/like', auth.requiresToken, post_controller.like)
    app.put('/api/posts/:id/dislike', auth.requiresToken, post_controller.dislike)
    app.get('/api/users/:id/posts', auth.requiresToken, post_controller.getUserPosts)
    app.delete('/api/post/:id', auth.requiresToken, post_controller.delete)

    app.get('/api/posts/:id/users', auth.requiresToken, post_controller.getPostUsers)
    app.get('/api/posts/tagged/user/:id', auth.requiresToken, post_controller.tagPost)

    app.post('/api/posts/getTrendingPosts', auth.requiresToken, post_controller.getTrendingPosts);
    app.post('/api/posts/getFollowedUsersPosts', auth.requiresToken, post_controller.getFollowedUsersPosts);
    app.post('/api/posts/reportsUserPost', auth.requiresToken, post_controller.reportsUserPost);
    
    /*=====================comment===============================*/

    app.post('/api/posts/:id/comments', auth.requiresToken, comment_controller.create)


    /*=====================notification===============================*/

    app.get('/api/notifications/getAllNotifications', auth.requiresToken, notification.getAllNotifications)
    app.post('/api/notifications/changeStatus/:id', auth.requiresToken, notification.changeStatus);
 };

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}