"use strict";

var config = {
    "development": {
        "db_server": {
            "database": "surphoDb",
            "host": "mongodb://localhost:9999/surphoDb"
            //  "host": "mongodb://localhost:27017/surphoDb"
        },
        "auth": {
            "secret": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9",
            "tokenPeriod": 1440
        },
        "web_server": {
            "url": "http://localhost:8000",
            "port": 8000
        },
        "push_notification": {
            // "serverKey": "AIzaSyBVpK42Xm23TiwvI3hVIP-m356uSYeChaw"
            "serverKey": "AIzaSyAUReWORiahQT87i2Yl2ZmdmPU-Zxm5hEA"
        }
    },
    "production": {
        "db_server": {
            "database": "surphoDb",
            "host": "mongodb://localhost:27017/surphoDb"
        },
        "auth": {
            "secret": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9",
            "tokenPeriod": 1440
        },
        "web_server": {
            "url": "http://206.189.140.127:8000",
            // "url": "http://localhost:8000",
            // "url": "http://ec2-13-58-21-133.us-east-2.compute.amazonaws.com:8000",
            "port": 8000
        },
        "push_notification": {
            // "serverKey": "AIzaSyBVpK42Xm23TiwvI3hVIP-m356uSYeChaw"
            "serverKey": "AIzaSyAUReWORiahQT87i2Yl2ZmdmPU-Zxm5hEA"
        }
    }
};

 //var node_env = process.env.NODE_ENV || 'development';
 var node_env = process.env.NODE_ENV || 'production';

module.exports = config[node_env];