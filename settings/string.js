"use strict";

 var localization= {
    NotFoundUser: {
        "English": "Resource Not Found",
        "French": "Ressource introuvable"
    },
    LoginSuccess: {
        "English": "Login Success",
        "French": "french logine introuvable"
    },
    UserDoneNotExist: {
        "English": "User Does Not Exist",
        "French": " L'utilisateur n'existe pas "
    },
    SocialId: {
        "English": "Social id is required.",
        "French": "L'identifiant social est requis."
    },
    VersionInDb: {
        "English": "version not found in DB.",
        "French": " version introuvables dans DB."
    },
    Mandatory: {
        "English": "Username is mandatory.",
        "French": "Nom d'utilisateur est obligatoire."
    },
    UsernameAlreadyExist: {
        "English": "Username already exists.",
        "French": "Ce nom d'utilisateur existe déjà."
    },
    UpdateProfile: {
        "English": "updatedMyProfile does not exist.",
        "French": "updatedMyProfile n'existe pas."
    },
    tokenDoneNotExist: {
        "English": "token profile does not exist.",
        "French": "le profil de jeton n'existe pas."
    },
    already: {
        "English": "already requested.",
        "French": "Déjà demandé."
    },
    NortificationError: {
        "English": "Error in inserting notifcation logs.",
        "French": "Erreur lors de l'insertion de journaux de notification."
    },
    privateProfileCase: {
        "English": "privateProfileCase.",
        "French": "privateProfileCase."
    },
    alreadyFollowing: {
        "English": "  already following.",
        "French": "déjà suivi."
    },
    insertingNotifcation: {
        "English": "Error in inserting notifcation logsg.",
        "French": "Erreur lors de l'insertion de journaux de notification."
    },
    unfollowSuccessful: {
        "English": " unfollow successful.",
        "French": "ne pas réussir."
    },
    UserBlocked: {
        "English": "User is blocked.",
        "French": "L'utilisateur est bloqué."
    },
    UserDeleted: {
        "English": "user deleted successfully",
        "French": "utilisateur supprimé avec succès."
    },
    PhoneVerified: {
        "English": "user phone verified",
        "French": "téléphone de l'utilisateur vérifié."
    },
   CantBlockHimself: {
        "English": "user cannot block himself",
        "French": "l'utilisateur ne peut pas se bloquer."
    },
    validStatus: {
         "English": "Not a valid status",
         "French": "l'utilisateur ne peut pas se bloquer."
     },
    
}

exports.getMessage= function  getMessage(language,messageKey){
    var message=makeMessage(language,messageKey);

    if(message == undefined ){
         message = makeMessage("English",messageKey);
    }


    if(message == undefined ){
        message = "Something went wrong";
   }
    return message;
}


 function makeMessage(language,messageKey){

    return messageKey[language];

 }

 exports.Keys=localization