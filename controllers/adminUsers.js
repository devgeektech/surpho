const models = require('mongoose').models;
const jwt = require('jsonwebtoken');
const async = require('async');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const FCM = require('fcm-node');
const config = require('../settings/config');
const response_helper = require('../helpers/response');
const authentication = require('../middleware/authentication');
const userLoginAndRegistration = require('../components/authorization')
const user_validate = require('../validators/user');
var FifoArray = require('fifo-array');
var bcrypt = require('bcrypt-nodejs');
const notification = require('../components/notificationLogs');
var androidPush = require('../components/androidNotification');
var iosPush = require('../components/iosNotification');
var mime = require('mime');

var concat = require('unique-concat');

//============================ view profile ======================//
exports.view_profile = function (req, res) {
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.params.id
            })
                .select('name email status blockedBy  blockedUsers pendingRequests description profilePic followers viewCounter following friends online views isPrivate isPhoneVerified role')
                .exec(function (err, user) {
                    if (err) return cb(err);
                    if (!user) return cb('user does not exist');
                    cb(null, user)
                })
        },


    ], function (err, user) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        var userData = {};
        userData.name = user.name;
        userData.email = user.email;
        userData.status = user.status;
        userData.description = user.description;
        userData.profilePic = user.profilePic;
        userData.followersCount = user.followers.length;
        userData.followingCount = user.following.length;
        userData.friendsCount = user.friends.length;
        userData.role = user.role;
        userData.created_at = user.created_at;
        userData.online = user.online;
        userData.views = user.views;
        return response.data(userData, 200);
    });
}
//===================================== End ==============================================//   


exports.change_user_status = function (req, res) {
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.body.id
            })
                .exec(function (err, user) {
                    if (err) return cb(err);
                    if (!user) return cb('post does not exist');
                    cb(null, user);
                });
        },
        function (user, cb) {
            if (req.body.status == 'true') {
                user.status = true;
            } else {
                user.status = false;
            }
            user.save(function (err, updatedUser) {
                if (err) return cb(updatedUser);
                cb(null, updatedUser);
            });
        },

    ], function (err, user) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(user, 200);
    })
};

var arraySpliceMethod = (array, userId) => {
    var index = array.indexOf(userId);
    if (index > -1) {
        array.splice(index, 1);
        array.save(function (err, result) {
            if (err) return response.failure(err, 400);
        })
    }
    return array;
}

exports.block_and_unblock = function (req, res) {
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.body.id
            })
                .exec(function (err, user) {
                    if (err) return cb(err);
                    if (!user) return cb('user does not exist');
                    var UserFollower, UersFollowing, UserFriend, UserPendingRequest;
                    var newArray = [];
                    var array = [];
                    newArray = newArray.concat(user.followers);
                    newArray = newArray.concat(user.following);
                    newArray = newArray.concat(user.friends);
                    newArray = newArray.concat(user.pendingRequests);
                    var resultedArry = _.uniq(newArray)
                    cb(null, user, resultedArry.toString().split(','));
                });
        },
        function (user, newArray, cb) {
            var array = [];
            async.eachSeries(newArray, function (singleUser, nextProfile) {
                if (req.body.isBlockedByAdmin = "true") {
                    models.User.findOne({ _id: singleUser })
                        .exec(function (err, fullUser) {
                            user.isBlockedByAdmin = true;
                            // let arr = fullUser.pendingRequests.toString()
                            // if (fullUser.pendingRequests.includes(req.body.id)) {
                            // }
                            models.User.findOneAndUpdate({ _id: singleUser }, {
                                $pull: {
                                    pendingRequests: req.body.id,
                                    friends: req.body.id,
                                    following: req.body.id,
                                    followers: req.body.id,
                                }
                            }, {
                                    new: true
                                }, (error, updatedData) => {
                                    if (err) cb(err);
                                    nextProfile();
                                })
                        })
                } else {
                    user.isBlockedByAdmin = false;
                    models.User.findOne({
                        _id: req.body.id
                    })
                        .exec(function (err, users) {
                            if (err) return cb(err);
                            if (!users) return cb('user does not exist');
                            users.followers = "",
                                users.following = "",
                                users.friends = "",
                                users.pendingRequests = "",
                                users.save(function (err, result) {
                                    if (err) return cb(err);
                                })
                        });
                    nextProfile()
                }
            }, function () {
                user.save(function (err, updatedUser) {
                    cb(err, updatedUser)
                })
            })
        },

    ], function (err, user) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(user, 200);
    })
};
//===============================================//


exports.search = function (req, res) {
    criteria = {
        $or: [
            { name: new RegExp(req.query.searchText, 'i') },
            { userName: new RegExp(req.query.searchText, 'i') }
        ]
    }
    models.User.findOne({ _id: req.user.id })
        .exec(function (err, userData) {
            models.User.find(criteria)
                .sort('created_at')
                .where('status').equals(true)
                .where('isBlockedByAdmin').equals(false)
                .exec(function (err, users) {
                    async.eachSeries(users, function (item, next) {
                        item._doc.isFollowing = item.addFollowingFlagSearch(item, userData);
                        item._doc.isFollower = item.addFollowersFlagSearch(item, userData);
                        item._doc.isFriend = item.addFriendsFlagSearch(item, userData);
                        item._doc.isRequested = item.addrequestFlagSearch(item, userData);

                        next()
                    }, function () {
                        models.User.count()
                            .exec(function (err, user) {
                                if (err) {
                                    return response.failure(err, 400);
                                } else if (!user) {
                                    return response.failure(err, 400, 'no users found')
                                } else {
                                    var resData = {
                                        items: users,
                                        length: users.length
                                    }
                                    var response = response_helper(res);
                                    response.data(resData, 200);
                                }
                            })
                    })
                })
        })
};

//=============================End============================//


exports.adminLogin = (req, res) => {
    var response = response_helper(res);
    var loginObject = {
        email: req.body.email,
        password: req.body.password
    }
    userLoginAndRegistration.login(loginObject, function (err, user) {
        if (err) {
            response.failure(err, 400);
        } else {
            response.data({ id: user._id, token: user.token }, 200);
        }
    })

}


exports.delete = function (req, res) {
    var response = response_helper(res);
    models.User.findOne({ _id: req.params.id }, function (err, user) {
        if (err) {
            return response.failure(err, 400);
        } else if (!user) {
            return response.failure(err, 400, 'User is not found');
        } else {
            user.remove(function (err, done) {
                if (err) {
                    return response.failure(err, 400);
                } else {
                    response.success('User is  deleted successfully.', 200);
                }
            })
        }
    });
};

exports.allUsers = function (req, res) {
    var query = {
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 10
    }
    var response = response_helper(res);
    models.User.find()
        .skip(query.skip > 0 ? ((query.skip - 1) * query.limit) : 0)
        .limit(query.limit)
        .exec(function (err, users) {
            if (err) {
                return response.failure(err, 400);
            } else if (!users) {
                return response.failure(err, 400, 'User is  not found.');
            } else {
                if (users.length > 0) {
                    models.User.find()
                        .exec(function (err, uCount) {
                            if (err) {
                                return response.failure(err, 400);
                            } else if (!uCount) {
                                return response.failure(err, 400, 'User is  not found.');
                            } else {
                                var usersAndCount = { users: users, count: uCount.length }
                                return response.data(usersAndCount, 200);
                            }
                        });
                } else {
                    var usersAndCount = { users: users, count: 0 }
                    return response.data(usersAndCount, 200);
                }


            }
        });
};
exports.getUserContact = function (req, res) {

    var response = response_helper(res);
    models.PhoneDirectory.find({ userId: req.params.id }, { _id: 0, name: 1, phone: 1 })
        .exec(function (err, contactNumbers) {
            if (err) {
                return response.failure(err, 400);
            } else if (!contactNumbers) {
                return response.failure(err, 400, 'Contact number is  not found.');
            } else {
                return response.data(contactNumbers, 200);
            }
        });
};
exports.signUp = function (req, res) {
    var response = response_helper(res);
    userLoginAndRegistration.userRegistration(req.body.email, req.body.username, req.body.password, function (err, user) {
        if (err) return response.failure(err, 400);
        response.data('user created', 200);
    });
};

