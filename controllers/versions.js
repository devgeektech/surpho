"use strict";

const models = require('mongoose').models;
const jwt = require('jsonwebtoken');
const async = require('async');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const FCM = require('fcm-node');
const config = require('../settings/config');
const response_helper = require('../helpers/response');
const authentication = require('../middleware/authentication');
const userLoginAndRegistration = require('../components/authorization')


exports.create = function(req, res) {
    async.waterfall([
        function(cb) {
            if(
                !req.body.normalVersion && req.body.normalVersion == 'undefined',
                !req.body.criticalVersion && req.body.criticalVersion == 'undefined'

            ) {
                cb('normalVersion and criticalVersion are required');
            } else {
                cb(null)
            }
        },
        function(cb) {
            var versionModel = new models.Version({
                normalAndroidVersion: req.body.normalAndroidVersion,
                criticalAndroidVersion: req.body.criticalAndroidVersion,
                normalIOSVersion: req.body.normalIOSVersion,
                criticalIOSVersion: req.body.criticalIOSVersion
            });
            versionModel.save(function(err, version) {
                if(err) return cb(err);
                cb(null, version);
            });
        }
    ], function(err, version) {
        var response = response_helper(res);
        if(err) return response.failure(err, 400);
        return response.data({
            id: version._id
        }, 200);    
    });
}



exports.get = function(req, res) {
    models.Version.findOne()
        .where('_id').equals(req.params.id)
        .select('normalAndroidVersion criticalAndroidVersion normalIOSVersion criticalIOSVersion')
            .exec(function(err, version) {
                var response = response_helper(res);
                if(err) return response.failure(err, 400);
                return response.data(version, 200);  
            });
};

exports.all = function(req, res) {
    models.Version.find()
        .select('normalAndroidVersion criticalAndroidVersion normalIOSVersion criticalIOSVersion')
        .exec(function(err, versions) {
            var response = response_helper(res);
            if(err) return response.failure(err, 400);
            return response.page(versions, 200);  
        });
};


exports.update = function (req, res) {
    async.waterfall([
        function(cb) {
            models.Version.findOne()
                .select('normalAndroidVersion criticalAndroidVersion normalIOSVersion criticalIOSVersion')
                .exec(function (err, version) {
                    if (err) return cb(err);
                    cb(null, version);
                });
        },
        function(version, cb) {
            version.normalAndroidVersion = req.body.normalAndroidVersion;
            version.criticalAndroidVersion = req.body.criticalAndroidVersion;
            version.normalIOSVersion = req.body.normalIOSVersion;
            version.criticalIOSVersion = req.body.criticalIOSVersion;
            version.save(function (err, updatedVersion) {
                if (err) return response.failure(err, 400);
                cb(null, updatedVersion);
            })
            
        }
    ], function (err, updatedVersion) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(updatedVersion, 200);
    });
}
