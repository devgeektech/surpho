"use strict";

const models = require('mongoose').models;
const jwt = require('jsonwebtoken');
const async = require('async');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const FCM = require('fcm-node');
const config = require('../settings/config');
const response_helper = require('../helpers/response');
const authentication = require('../middleware/authentication');
const userLoginAndRegistration = require('../components/authorization');
const notification = require('../components/notificationLogs')
const postCreate = require('../validators/post');
const moment = require('moment');
var concat = require('unique-concat');
var androidPush = require('../components/androidNotification');
const iosPush = require('../components/iosNotification');


var multiPostsModel = function (posts) {
    return {
        _id: posts._id,
        createdBy: posts.createdBy,
        updated_at: posts.updated_at,
        created_at: posts.created_at,
        likes: posts.likes,
        views: posts.views,
        media: posts.media,
        status: posts.status,
        isBlocked: posts.isBlocked,
        description: posts.description,
        title: posts.title,
        tags: posts.tags,
        commentsCount: posts.comments.length,
        viewsCount: posts.views.length,
        likesCount: posts.likes.length,
        isLiked: posts._doc.isLiked,
        viewCounter: posts._doc.viewCounter
    }
}


exports.create = function (req, res) {
    var androidDeviceId = [];
    var iosDeviceId = [];
    async.waterfall([
        function (cb) {
            postCreate.post_form(req.body).is_valid(function (err, form) {
                if (err) return cb(err);
                if (!form.data.media.length)
                    return cb('atleast a media file is required')
                cb(null, form.data);
            });
        },
        function (form, cb) {
            models.User.find({
                _id: req.user.id
            })
                .select('name')
                .exec(function (err, user) {
                    if (err) return cb(err);
                    cb(null, form, user);
                })
        },
        function (form, user, cb) {
            form.createdBy = req.user.id;
            form.tags = form.tags;
            var post = new models.Post(form);
            post.save(function (err, post) {
                if (err) return cb(err);
                cb(null, post, user);
            });
        },
        function (post, user, cb) {
            async.eachSeries(post.tags, (i, seriesCallback) => {
                models.User.find({
                    _id: i
                })
                    .exec(function (err, response) {
                        var device = response[0].device;
                        if (err) return cb(err)
                        if (device.deviceType == "android") {
                            androidDeviceId.push(device.deviceId);
                        } else if (device.deviceType == "ios") {
                            iosDeviceId.push(device.deviceId);
                        }
                        seriesCallback();
                    })
            }, function (err, userDevice) {
                cb(null, post, user, userDevice);
            })
        },
        function (post, user, userDevice, cb) {
            if (post.tags) {
                var message = user[0].name + " has tagged you in a post.";
                var type = "tagOnPost";
                var title = "Tagged in a post";
                async.eachSeries(post.tags, (i, seriesCallback) => {
                    notification.create(i, req.user.id, post.id, type, title, message, (err, response) => {
                        if (err) {
                            return cb("Error in inserting tagged notification");
                        } else {
                            seriesCallback();
                        }
                    })
                }, function () {
                    var data = {};
                    data.message = message;
                    data.title = title;
                    data.type = type;
                    data.actionId = post.id;

                    androidPush.androidPushNotification(androidDeviceId, data, (err) => { })

                    // iosPush.apn(iosDeviceId, data, (err) => {
                    // })
                    cb(null, post);
                })
            } else {
                cb(null, post)
            }
        },
    ], function (err, post) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(post, 200);
    })
}


exports.getUserPosts = function (req, res) {
    var query = {
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 30,
    }
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.params.id
            })
                .where('isBlockedByAdmin').equals(false)
                .exec(function (err, user) {
                    if (err) return cb(err)
                    if (!user) return cb('user does not exists')
                    cb(null, user);
                })
        },
        function (user, cb) {
            var blockUserList = user.blockedBy.concat(user.blockedUsers);
            var mongoQuery = models.Post.find({
                'createdBy': {
                    $nin: blockUserList
                }
            })
            var countQuery = models.Post.count()
            mongoQuery.skip(query.skip)
            mongoQuery.limit(query.limit)
            mongoQuery.sort({
                created_at: -1
            })
            mongoQuery.where('status').equals(true)
            mongoQuery.where('isBlocked').equals(false)
            mongoQuery.populate({
                path: 'comments',
                select: 'name userName _id profilePic'
            })
            mongoQuery.populate({
                path: 'tags',
                select: 'name userName _id profilePic'
            })
            mongoQuery.populate({
                path: 'createdBy',
                as: 'user',
                select: 'name profilePic _id userName'
            })

            // mongoQuery.where('status').equals(true)

            countQuery.skip(query.skip)
            countQuery.limit(query.limit)
            countQuery.sort({
                created_at: -1
            })
            countQuery.where('status').equals(true)
            countQuery.where('isBlocked').equals(false)

            mongoQuery.where("createdBy").nin(req.user.id)
            mongoQuery.where("createdBy").equals(req.params.id)
            countQuery.where("createdBy").equals(req.params.id)


            mongoQuery.exec(function (err, posts) {
                if (err) return cb(err);
                if (!posts) return cb('No post Found!')
                cb(null, posts, countQuery);
            })
        },
        function (posts, countQuery, cb) {
            async.eachSeries(posts, function (singlePost, next) {
                singlePost._doc.isLiked = singlePost.addlike(singlePost, req.user.id);
                var counts = singlePost.counts(singlePost);
                singlePost._doc.likesCount = counts.likesCount;
                singlePost._doc.viewsCount = counts.viewsCount;
                singlePost._doc.commentsCount = counts.commentsCount;
                next();
            }, function (err) {
                if (err) return cb(err);
                cb(null, posts, countQuery)
            });
        },
        function (posts, countQuery, cb) {
            models.Post.find({
                createdBy: req.params.id
            })
                .exec(function (err, resultedData) {
                    cb(null, posts, countQuery, resultedData)
                })
        },
        function (posts, countQuery, resultedData, cb) {
            countQuery
                .exec(function (err, count) {
                    cb(null, posts, count, resultedData);
                });
        }
    ], function (err, posts, count, resultedData) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.page(posts, 200, resultedData.length, query.skip);
    })
}


exports.all = function (req, res) {
    var query = {
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 100
    }
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.user.id
            })
                .exec(function (err, user) {
                    if (err) return cb(err)
                    if (!user) return cb('user does not exists')
                    cb(null, user);
                })
        },
        function (user, cb) {
            models.User.find({
                $or: [{
                    status: false
                }, {
                    isBlockedByAdmin: true
                }]
            })
                .select('_id status isBlocked')
                .exec(function (err, allUserData) {
                    if (err) return cb(err);
                    cb(null, user, allUserData);
                })
        },
        function (user, allUserData, cb) {
            var blockUserList = []
            var blockUserList1 = []
            async.eachSeries(allUserData, function (item, next) {
                blockUserList.push(item._id);
                next()
            }, function () {
                blockUserList1 = user.blockedBy.concat(user.blockedUsers);
                blockUserList = blockUserList.concat(blockUserList1);
                user.following.push(user._id);
                models.Post.find({
                    'createdBy': {
                        $nin: blockUserList
                    }
                })
                    .where('status').equals(true)
                    .where('isBlocked').equals(false)
                    .where('createdBy').in(user.following)

                    .skip(query.skip)
                    .limit(query.limit)
                    .sort({
                        created_at: -1
                    })
                    .populate({
                        path: 'createdBy',
                        select: 'name profilePic _id userName',
                        options: {
                            limit: 5
                        }
                    })
                    .populate({
                        path: 'tags',
                        select: 'name profilePic _id userName'
                    })
                    .exec(function (err, posts) {
                        if (err) return cb(err);
                        cb(null, posts, user,blockUserList);
                    })
            })
        },
        function (posts, user,blockUserList, cb) {
            async.eachSeries(posts, function (singlePost, next) {
                singlePost._doc.isLiked = singlePost.addlike(singlePost, req.user.id);
                var counts = singlePost.counts(singlePost);
                singlePost._doc.likesCount = counts.likesCount;
                singlePost._doc.viewsCount = counts.viewsCount;
                singlePost._doc.commentsCount = counts.commentsCount;
                singlePost._doc = multiPostsModel(singlePost);
                next();
            }, function (err) {
                if (err) return cb(err);
                cb(null, posts, user,blockUserList)
            });
        },
 
        function (posts, user,blockUserList, cb) {
            models.Post.count()
                .sort({
                    created_at: -1
                })
                .where('createdBy').nin(user.blockedUsers)
                .where('createdBy').nin(user.blockedBy)
                .where('createdBy').in(user.following)
                .where('createdBy').nin(blockUserList)
                .where('status').equals(true)
                .where('isBlocked').equals(false)
                .exec(function (err, count) {
                    cb(null, posts, count);
                });
        }
    ], function (err, posts, count) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.page(posts, 200, count, query.skip);
    })
}

exports.update = function (req, res) {
    async.waterfall([
        function (cb) {
            models.Post.findOne({
                _id: req.params.id
            })
                .exec(function (err, post) {
                    if (err) return cb(err);
                    if (!post) return cb('post does not exist');
                    cb(null, post);
                });
        },
        function (post, cb) {
            post.title = req.body.title || post.title;
            post.description = req.body.description || post.description;
            post.media = req.body.images || post.media;
            post.tags = req.body.tags || post.tags;
            post.save(function (err, updatedPost) {
                if (err) return cb(updatedPost);
                cb(null, updatedPost);
            });
        },
    ], function (err, post) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(post, 200);
    })
};

exports.postList = function (req, res) {
    var query = {
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 10
    }
    async.waterfall([
        function (cb) {
            models.Post.find()
                .skip(query.skip > 0 ? ((query.skip - 1) * query.limit) : 0)
                .limit(query.limit)
                .populate({
                    path: 'createdBy',
                    select: 'name profilePic _id userName'
                })
                .exec(function (err, posts) {
                    if (err) return cb(err);
                    cb(null, posts);
                })
        },
        function (posts, cb) {
            async.eachSeries(posts, function (singlePost, next) {
                singlePost._doc.isLiked = singlePost.addlike(singlePost, req.user.id);
                var counts = singlePost.counts(singlePost);
                singlePost._doc.likesCount = counts.likesCount;
                singlePost._doc.viewsCount = counts.viewsCount;
                singlePost._doc.commentsCount = counts.commentsCount;
                singlePost._doc = multiPostsModel(singlePost);
                next();
            }, function (err) {
                if (err) return cb(err);
                cb(null, posts)
            });
        },
        function (posts, cb) {
            models.Post.count()
                .exec(function (err, count) {
                    cb(null, posts, count);
                });
        }
    ], function (err, posts, count) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.page(posts, 200, count, query.skip);
    })
}

exports.change_status = function (req, res) {
    async.waterfall([
        function (cb) {
            models.Post.findOne({
                _id: req.body.id
            })
                .exec(function (err, post) {
                    if (err) return cb(err);
                    if (!post) return cb('post does not exist');
                    cb(null, post);
                });
        },
        function (post, cb) {
            if (req.body.status == 'true') {
                post.status = true;
            } else {
                post.status = false;
            }
            post.save(function (err, updatedPost) {
                if (err) return cb(updatedPost);
                cb(null, updatedPost);
            });
        },

    ], function (err, post) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(post, 200);
    })
};

exports.block_and_unblock = function (req, res) {
    async.waterfall([
        function (cb) {
            models.Post.findOne({
                _id: req.body.id
            })
                .exec(function (err, post) {
                    if (err) return cb(err);
                    if (!post) return cb('post does not exist');
                    cb(null, post);
                });
        },
        function (post, cb) {
            if (req.body.isBlocked == 'true') {
                post.isBlocked = true;
            } else {
                post.isBlocked = false;
            }
            post.save(function (err, updatedPost) {
                if (err) return cb(updatedPost);
                cb(null, updatedPost);
            });
        },

    ], function (err, post) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(post, 200);
    })
};
exports.post_search = function (req, res) {
    models.Post.find({
        title: new RegExp(req.query.searchText, 'i')
    })
        .sort('created_at')
        .populate({
            path: 'createdBy',
            select: 'name profilePic _id userName'
        })
        .exec(function (err, posts) {
            var response = response_helper(res);
            if (err) return response.failure(err, 400);
            else if (!posts) return response.failure(err, 400, 'no post found')
            else return response.data(posts, 200);
        })
};


exports.like = function (req, res) {
    var deviceId, device;
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.user.id
            })
                .select('name')
                .exec(function (err, user) {
                    if (err) return cb(err);
                    cb(null, user);
                })
        },
        function (user, cb) {
            models.Post.findOneAndUpdate({
                _id: req.params.id
            }, {
                    $addToSet: {
                        likes: req.user.id
                    }
                }, {
                    new: true
                }, function (err, post) {
                    if (err) return cb(err);
                    if (!post) return cb('post does not exist');
                    models.User.findOne({
                        _id: post.createdBy
                    })
                        .exec(function (err, response) {
                            device = response.device;
                            if (err) return cb(err)
                            deviceId = device.deviceId;
                            var message = user.name + " has liked your post.";
                            var type = "likePost";
                            var title = "Post Liked";
                            if (req.user.id != post.createdBy) {
                                notification.create(post.createdBy, req.user.id, req.params.id, type, title, message, (err, response) => {
                                    if (err) {
                                        return cb("Error in inserting like notification");
                                    } else {
                                        var data = {};
                                        data.message = message;
                                        data.title = title;
                                        data.type = type;
                                        data.actionId = req.params.id;
                                        if (device.deviceType == "android") {
                                            androidPush.androidPushNotification(deviceId, data, (err) => {

                                            })
                                        } else if (device.deviceType == "ios") {
                                            iosPush.apn(deviceId, data, (err) => {

                                            })
                                        } else {
                                            console.log("No valid device type.")
                                        }
                                        cb(null, post);
                                    }
                                })
                            } else {
                                cb(null, post);
                            }
                        })

                })
            /*==============notification of like ==================*/
        },
        function (singlePost, cb) {
            var counts = singlePost.counts(singlePost);
            singlePost._doc.likesCount = counts.likesCount;
            singlePost._doc.viewsCount = counts.viewsCount;
            singlePost._doc.commentsCount = counts.commentsCount;
            singlePost._doc.isLiked = singlePost.addlike(singlePost, req.user.id);
            cb(null, singlePost._doc);
        }
    ], function (err, post) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(post, 200);
    });
}


exports.dislike = function (req, res) {
    /*==============notification of like ==================*/
    async.waterfall([
        function (cb) {
            models.Post.findOneAndUpdate({
                _id: req.params.id
            }, {
                    $pull: {
                        likes: req.user.id
                    }
                }, {
                    new: true
                }, function (err, post) {
                    if (err) return cb(err);
                    if (!post) return cb('post does not exist');
                    cb(null, post);
                })

        },
        function (singlePost, cb) {
            var counts = singlePost.counts(singlePost);
            singlePost._doc.likesCount = counts.likesCount;
            singlePost._doc.viewsCount = counts.viewsCount;
            singlePost._doc.commentsCount = counts.commentsCount;
            singlePost._doc.isLiked = singlePost.addlike(singlePost, req.user.id);
            cb(null, singlePost._doc);
        }
    ], function (err, post) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(post, 200);
    })
}



exports.getPostUsers = function (req, res) {
    var query = {
        type: req.query.type || 'users',
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 10
    }
    async.waterfall([
        function (cb) {
            models.Post.findOne({
                _id: req.params.id
            })
                .exec(function (err, post) {
                    if (err) return cb(err);
                    if (!post) return cb('post does not exist')
                    cb(null, post);
                })

        },
        function (post, cb) {
            var criteria = {};
            if (query.type == "views") {
                criteria = {
                    _id: {
                        $in: post.views
                    }
                }
            }
            if (query.type == "likes") {
                criteria = {
                    _id: {
                        $in: post.likes
                    }
                }
            }
            var mongoQuery = models.User.find(criteria);
            var countQuery = models.User.count(criteria);

            mongoQuery.skip(query.skip)
            mongoQuery.limit(query.limit)
            mongoQuery.select('name userName profilePic')


            countQuery.skip(query.skip)
            countQuery.limit(query.limit)
            countQuery.select('name userName profilePic')

            mongoQuery.exec(function (err, users) {
                if (err) {
                    if (err.message.indexOf('Cast to ObjectId failed') !== -1)
                        return cb(null, [], countQuery);
                }
                cb(null, users, countQuery);
            });
        },
        function (users, countQuery, cb) {
            countQuery
                .exec(function (err, count) {
                    cb(null, users, count);
                });
        }
    ], function (err, users, count) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.page(users, 200, count, query.skip);
    })
}


exports.get = function (req, res) {
    async.waterfall([
        function (cb) {
            models.Post.findOneAndUpdate({
                _id: req.params.id
            }, {
                    $addToSet: {
                        views: req.user.id,
                        'viewsTrending': {
                            'viewedBy': req.user.id,
                            'viewTime': moment()
                        }
                    }
                }, {
                    new: true
                })
                .populate({
                    path: "comments",
                    populate: {
                        path: "createdBy",
                        select: "name profilePic"
                    }
                })
                .populate({
                    path: 'createdBy',
                    select: 'name userName _id profilePic'
                })
                .populate({
                    path: 'tags',
                    select: 'name userName _id profilePic'
                })
                .exec(function (err, post) {
                    if (err) return cb(err);
                    if (!post) return cb('post does not exist');
                    if (!post.status) return cb('post has been deleted');
                    cb(null, post);
                })
        },
        function (post, cb) {
            var postDate = moment(post.created_at).format('YYYY MM DD HH:mm:ss');
            var currentDate = moment(new Date()).format('YYYY MM DD HH:mm:ss');
            var beforeDate = moment(currentDate).subtract(10, 'days').format('YYYY MM DD HH:mm:ss');
            if (postDate > beforeDate) {
                post.viewCounter += 1;
                post.save(function (err, updatedPost) {
                    if (err) return cb(err);
                    cb(null, updatedPost);
                });
            } else {
                cb(null, post);
            }
        },
        function (singlePost, cb) {
            singlePost._doc.isLiked = singlePost.addlike(singlePost, req.user.id);
            var counts = singlePost.counts(singlePost);
            singlePost._doc.likesCount = counts.likesCount;
            singlePost._doc.viewsCount = counts.viewsCount;
            singlePost._doc.commentsCount = counts.commentsCount;

            cb(null, singlePost._doc);
        }
    ], function (err, post) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(post, 200);
    })
}

exports.delete = function (req, res) {
    var response = response_helper(res);
    models.Post.findOne({
        _id: req.params.id
    }, function (err, post) {
        if (err) {
            return response.failure(err, 400);
        } else if (!post) {
            return response.failure(err, 400, 'Post is not found');
        } else {
            post.remove(function (err, done) {
                if (err) {
                    return response.failure(err, 400);
                } else {
                    response.success('Post is  deleted successfully.', 200);
                }
            })
        }
    });
};


//============================Start tagPost===========================//

exports.tagPost = function (req, res) {
    var query = {
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 10
    }
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.user.id,
            })
                .exec(function (err, user) {
                    if (err) return cb(err)
                    if (!user) return cb('user does not exists')
                    cb(null, user);
                })
        },
        function (user, cb) {
            models.User.find({
                status: false
            })
                .exec(function (err, result) {
                   
                    if (err) return cb(err)
                    if (!result) return cb('users does not exists');
                    var array = [];
                    result.forEach(function (item) {
                        array.push(item._id);

                    })
                    cb(null, user, array);
                })
        },
        function (user, array, cb) {
            var blockUserList = user.blockedBy.concat(user.blockedUsers);
            var blockUserList2 = array.concat(blockUserList);
            models.Post.find({
                tags: req.params.id
            })
                .skip(query.skip)
                .limit(query.limit)
                .sort({
                    created_at: -1
                })
                .populate({
                    path: 'createdBy',
                    select: 'name profilePic _id userName'
                })
                .populate({
                    path: 'tags',
                    select: 'name userName _id profilePic'
                })
                .where('status').equals(true)
                .where('createdBy').nin(blockUserList2)
                .where('isBlocked').equals(false)
                .exec(function (err, posts) {
                    if (err) return cb(err);
                    cb(null, posts, user);
                })
        },
        function (posts, user, cb) {
            async.eachSeries(posts, function (singlePost, next) {
                singlePost._doc.isLiked = singlePost.addlike(singlePost, req.user.id);
                var counts = singlePost.counts(singlePost);
                singlePost._doc.likesCount = counts.likesCount;
                singlePost._doc.viewsCount = counts.viewsCount;
                singlePost._doc.commentsCount = counts.commentsCount;
                singlePost._doc = multiPostsModel(singlePost);
                next();
            }, function (err) {
                if (err) return cb(err);
                cb(null, posts, user)
            });
        },
        function (posts, user, cb) {
            models.Post.count({
                tags: req.params.id
            })
                .sort({
                    created_at: -1
                })
                .where('createdBy').nin(user.blockedUsers)
                .where('status').equals(true)
                .where('isBlocked').equals(false)

                .exec(function (err, count) {
                    cb(null, posts, count);
                });
        }
    ], function (err, posts, count) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.page(posts, 200, count, query.skip);
    })
}

//============================End tagPost============================// 


/* ============================
 * TRENDING POSTS
 * ===========================*/

exports.getTrendingPosts = function (req, res) {
    var query = {
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 100,
        day: parseInt(req.query.day) || 10
    }
    var currentDate = moment(new Date()).format('YYYY MM DD HH:mm:ss');
    var beforeDate = moment(currentDate).subtract(query.day, 'days').format('YYYY MM DD HH:mm:ss');
    async.waterfall([
        //==========//
        function (cb) {
            models.User.findOne({
                _id: req.user.id
            })
                .exec(function (err, user) {
                    if (err) return cb(err)
                    if (!user) return cb('user does not exists')
                    cb(null, user);
                })
        },
        function (user, cb) {
            models.User.find({
                $or : [
                    { $and : [ { status : false }, { isBlockedByAdmin : true} ] },
                    { $and : [ { status : false }, { isBlockedByAdmin : false } ] },
                    { $and : [ { status : true }, { isBlockedByAdmin :  true} ] }
                ]
            })
                .exec(function (err, allUserData) {
                    if (err) return cb(err);
                    cb(null, user, allUserData);
                })
        },
        function (user, allUserData, cb) {
            var blockUserList = []
            var blockUserList1 = []
            for (var i in allUserData) {
                blockUserList.push(allUserData[i]._id);
            }
            blockUserList1 = user.blockedBy.concat(user.blockedUsers);
            blockUserList = blockUserList.concat(blockUserList1);
            //==========//
            
            models.Post.find({
                'createdBy': {
                    $nin: blockUserList
                }
            })
                .skip(query.skip)
                .limit(query.limit)
                .sort({
                    viewCounter: -1
                })
                .populate({
                    path: 'createdBy',
                    select: 'name profilePic viewCounter'
                })
                .populate({
                    path: 'tags',
                    select: 'name userName _id profilePic'
                })
                .where('status').equals(true)
                .where('isBlocked').equals(false)

                .exec(function (err, posts) {
                    if (err) return cb(err);
                    cb(null, posts);
                })
        },
        function (result, cb) {
            var trendingPosts = [];
            var counter = [];
            async.eachSeries(result, (i, seriesCallback) => {
                counter.push(i.viewCounter);
                i._doc.isLiked = i.addlike(i, req.user.id);
                var counts = i.counts(i);
                i._doc.likesCount = counts.likesCount;
                i._doc.viewsCount = counts.viewsCount;
                i._doc.commentsCount = counts.commentsCount;
                i._doc = multiPostsModel(i);
                trendingPosts.push(i);
                seriesCallback();

            }, (err, response) => {
                cb(null, trendingPosts);
            })
        },
        function (posts, cb) {
            models.Post.count()
                .exec(function (err, count) {
                    cb(null, posts, count);
                });
        }
    ], function (err, posts, count) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.page(posts, 200, count, query.skip);
    })
}

exports.getFollowedUsersPosts = function (req, res) {
    var query = {
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 10,
    }
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.user.id
            })
                .exec(function (err, user) {
                    if (err) return cb(err)
                    if (!user) return cb('User not found.');
                    cb(null, user);
                })
        },
        function (user, cb) {
            var blockUserList = user.blockedBy.concat(user.blockedUsers);
            var mongoQuery = models.Post.find({
                'createdBy': {
                    $nin: blockUserList
                }
            })
            var countQuery = models.Post.count()
            mongoQuery.skip(query.skip)
            mongoQuery.limit(query.limit)
            mongoQuery.sort({
                created_at: -1
            })
            mongoQuery.where('createdBy').in(user.following);
            mongoQuery.where('isBlocked').equals(false)


            countQuery.skip(query.skip)
            countQuery.limit(query.limit)
            countQuery.sort({
                created_at: -1
            })
            countQuery.where('createdBy').in(user.following);
            countQuery.where('isBlocked').equals(false)


            mongoQuery.exec(function (err, posts) {
                if (err) return cb(err);
                cb(null, posts, countQuery);
            })
        },
        function (posts, countQuery, cb) {
            async.eachSeries(posts, function (singlePost, next) {
                singlePost._doc.isLiked = singlePost.addlike(singlePost, req.user.id);
                var counts = singlePost.counts(singlePost);
                singlePost._doc.likesCount = counts.likesCount;
                singlePost._doc.viewsCount = counts.viewsCount;
                singlePost._doc.commentsCount = counts.commentsCount;
                next();
            }, function (err) {
                if (err) return cb(err);
                cb(null, posts, countQuery)
            });
        },
        function (posts, countQuery, cb) {
            countQuery
                .exec(function (err, count) {
                    cb(null, posts, count);
                });
        }
    ], function (err, posts, count) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.page(posts, 200, count, query.skip);
    })
}


// @report user
// method post
// body ===> post id
exports.reportsUserPost = (req, res) => {
    const response = response_helper(res);
    const data = {};
    models.Post.findById(
        req.body.id
    )
        .populate({
            path: 'createdBy'
        })
        .then(post => {
            if (!post) return response.failure('No such post', 400)
            if (post.createdBy.id === req.user.id) return response.failure('You cannot report your own post.', 400, 'You cannot report your own post.')
            let temp = post.reports.toString().split(',')
            if (temp.includes(req.user.id)) return response.failure('Post is already reported by you.', 400, 'Post is already reported by you.')
            if (post.reports.length >= 10) return response.failure('Post is blocked.', 400., 'Post is blocked.')
            if (post.reports.length === 9) {
                post.update({
                    $addToSet: {
                        reports: req.user.id
                    },
                    isBlocked: true
                }).then(savedPost => {
                    data.message = 'One of Your post have been blocked for the content.';
                    data.title = 'Report In Post';
                    data.type = 'Blocked';
                    data.actionId = post.id;
                    if (post.createdBy.device.deviceType == 'android') {
                        androidPush.androidPushNotification(post.createdBy.device.deviceId, data, function (message) {
                            response.success('Post is  blocked for the content.', 200);
                        })
                    }
                    if (post.createdBy.device.deviceType == 'ios') {
                        iosPush.apn(post.createdBy.device.deviceId, data, function (message) {
                            response.success('Post is  blocked for the content.', 200);
                        })
                    }
                }).catch(err => response.failure(err, 400))
            } else {
                var messageData = 'One of Your post have been reported for the content please review your post.';
                var titleData = 'Report In Post';
                var typeData = 'Report';
                post.update({
                    $push: {
                        reports: req.user.id
                    }
                }).then(savedPost => {
                    notification.create(post.createdBy, req.user.id, req.body.id, data.type, titleData, messageData, (err, response) => {
                        if (err) {
                            return cb("Error in inserting like notification");
                        } else {
                            data.message = messageData;
                            data.title = titleData;
                            data.type = typeData;
                            data.actionId = post.id
                            if (post.createdBy.device.deviceType === 'android') {
                                androidPush.androidPushNotification(post.createdBy.device.deviceId, data, function (message) {
                                    response.success('Post is  Reported for the content.', 200);
                                })
                            }
                            if (post.createdBy.device.deviceType === 'ios') {
                                iosPush.apn(post.createdBy.device.deviceId, data, function (message) {
                                    response.success('Post is  Reported for the content.', 200);
                                })
                            }
                        }
                    })
                }).catch(err => response.failure(err, 400))
            }
        }).catch(err => response.failure(err, 400))

}
