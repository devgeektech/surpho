"use strict";

const models = require('mongoose').models;
const jwt = require('jsonwebtoken');
const async = require('async');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
var Jimp = require("jimp");
const _ = require('lodash');
const FCM = require('fcm-node');
const formidable = require('formidable');
const moment = require('moment');
const config = require('../settings/config');
const response_helper = require('../helpers/response');
const authentication = require('../middleware/authentication');
const userLoginAndRegistration = require('../components/authorization');
const uuidv4 = require('uuid/v4');
var mime = require('mime'); 

/*====================string Replace=========================*/


/* Define function for escaping user input to be treated as 
   a literal string within a regular expression */
function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

/* Define functin to find and replace specified term with replacement string */
function replaceAll(str, term, replacement) {
    return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
}






exports.upload = function (req, res) {
    var fileData = req.files[0];
    async.waterfall([
        function(cb) {
                if(fileData) {
                    if (req.body.mediaType == "image") {
                        var fileName = fileData.filename;
                        var newStr = "./uploads/" + fileName
                            Jimp.read(fileData.path, function (err, lenna) {
                            if(err) return cb(err);
                            lenna.write(newStr); // save 
                            cb(null, { mediaUrl: config.web_server.url + "/uploads/" + fileName, mediaType: req.body.mediaType || "defaultMedia"});
                        });
                    }
                    if(req.body.mediaType == "video") {
                        var fileName = fileData.filename;
                        var newPath = "./uploads/" + fileName;
                            cb(null, { mediaUrl: config.web_server.url + "/uploads/" + fileName, mediaType: req.body.mediaType || "defaultMedia"});
                    };
                } else {
                    cb('media is required');
                };
        }
    ], function (err, media) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(media, 200);
    });
}