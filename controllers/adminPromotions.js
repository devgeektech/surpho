"use strict";
const async = require('async');
const models = require('mongoose').models;
const response_helper = require('../helpers/response');
const mongoose = require('mongoose');
const _ = require('lodash');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
var config = require('../settings/config');

//==========================================Start createPromotion============================================//

exports.create = (req, res) => {
    var response = response_helper(res);
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            callback(null, './uploads');
        },
        filename: function (req, file, callback) {
            var fileName = path.parse(file.originalname).name;
            callback(null, fileName + '-' + Date.now() + path.extname(file.originalname))
        }
    });
    var upload = multer({
        storage: storage
    }).any();
    upload(req, res, function (err) {

        if (err) {
            res.status(400).send(err);
        } else {
            let obj = {};
            if (req.files) {
                obj.image = [];
                obj.video = [];
                if (req.files) {
                    _.forEach(req.files, function (file) {
                        var ext = path.extname(file.originalname)
                        if (ext == '.jpg' || ext == '.png' || ext == '.jpg') {
                            obj.image.push({ 'image_file': config.web_server.url + '/uploads/' + file.filename });
                        } else if (ext == '.avi' || ext == '.mov' || ext == '.wmv' || ext == '.mp4') {
                            obj.video.push({ video_file: config.web_server.url + '/uploads/' + file.filename });
                        }
                    })
                }
            }

            obj.promotionText = req.body.promotionText || '';
            obj.promoLocation = { type: "Point", coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.latitude)] };
            obj.createdBy = req.user.id;
            obj.isBlocked = false;
            obj.status = true;
            var promotion = new models.Promotion(obj);
            promotion.save(function (err, result) {
                if (err) {
                    return response.failure(err, 400);
                } else {
                    return response.data(result, 200);
                }
            });

        }
    });




}
//==========================================End createPromotion============================================//

//========================================= All Promotions ================================================//

exports.all = function (req, res) {
    var query = {
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 10
    }
    var response = response_helper(res);
    models.Promotion.find()
        .skip(query.skip > 0 ? ((query.skip - 1) * query.limit) : 0)
        .limit(query.limit)
        .exec(function (err, promotions) {
            if (err) {
                return response.failure(err, 400);
            } else if (!promotions) {
                return response.failure(err, 400, 'Promotion is  not found.');
            } else {
                models.Promotion.find()
                    .exec(function (err, procount) {
                        if (err) {
                            return response.failure(err, 400);
                        } else if (!promotions) {
                            return response.failure(err, 400, 'Promotion is  not found.');
                        } else {
                            var promotionsandcount = { promotions: promotions, count: procount.length }
                            return response.data(promotionsandcount, 200);
                        }
                    });

            }
        });
};

exports.getPromotion = function (req, res) {
    var response = response_helper(res);
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        return response.failure('Not a valid _id', 400, 'id is invalid')
    }
    models.Promotion.findOne({ _id: req.params.id })
        .exec(function (err, promotion) {
            if (err) {
                return response.failure(err, 400);
            } else if (!promotion) {
                return response.failure(err, 400, 'Promotion is  not found.');
            } else {
                return response.data(promotion, 200);
            }
        });
};

//========================================= End Promotion ==================================================

exports.block_and_unblock = function (req, res) {
    async.waterfall([
        function (cb) {
            models.Promotion.findOne({
                _id: req.body.id
            })
                .exec(function (err, promotion) {
                    if (err) return cb(err);
                    if (!promotion) return cb('Promotion does not exist');
                    cb(null, promotion);
                });
        },
        function (promotion, cb) {
            if (req.body.isBlocked == 'true') {
                promotion.isBlocked = true;
            } else {
                promotion.isBlocked = false;
            }
            promotion.save(function (err, updatedPromotion) {
                if (err) return cb(updatedPromotion);
                cb(null, updatedPromotion);
            });
        },

    ], function (err, promotion) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(promotion, 200);
    })
};



//==========================================Start deletePromotion============================================//

exports.delete = function (req, res) {
    var response = response_helper(res);
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        return response.failure('Not a valid _id', 400, 'id is invalid')
    }
    models.Promotion.findOne({ _id: req.params.id }, function (err, promotion) {
        if (err) {
            return response.failure(err, 400);
        } else if (!promotion) {
            return response.failure(err, 400, 'Promotion is not found');
        } else {
            promotion.remove(function (err, done) {
                if (err) {
                    return response.failure(err, 400);
                } else {
                    response.success('Promotion is  deleted successfully.', 200);
                }
            })
        }
    });
};

//==========================================End deletePromotion============================================//

exports.change_promotion_status = function (req, res) {
    async.waterfall([
        function (cb) {
            models.Promotion.findOne({
                _id: req.body.id
            })
                .exec(function (err, promotion) {
                    if (err) return cb(err);
                    if (!promotion) return cb('Promotion does not exist');
                    cb(null, promotion);
                });
        },
        function (promotion, cb) {
            if (req.body.status == 'true') {
                promotion.status = true;
            } else {
                promotion.status = false;
            }
            promotion.save(function (err, updatedPromotion) {
                if (err) return cb(updatedPromotion);
                cb(null, updatedPromotion);
            });
        },

    ], function (err, promotion) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(promotion, 200);
    })
};



//==========================================Start updatePromotion============================================//

exports.update = function (req, res) {
    var response = response_helper(res);
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            callback(null, './uploads');
        },
        filename: function (req, file, callback) {
            var fileName = path.parse(file.originalname).name;
            callback(null, fileName + '-' + Date.now() + path.extname(file.originalname))
        }
    });
    var upload = multer({
        storage: storage
    }).any();
    upload(req, res, function (err) {

        if (err) {
            res.status(400).send(err);
        } else {
            models.Promotion.findOne({ _id: req.body.id }, function (err, promotion) {
                if (err) {
                    return response.failure(err, 400);
                } else if (!promotion) {
                    return response.failure(err, 400, 'Promotion is  not found.');
                } else {
                    if (req.files) {
                        if (req.files) {
                            _.forEach(req.files, function (file) {
                                var ext = path.extname(file.originalname)
                                if (ext == '.jpg' || ext == '.png' || ext == '.jpg') {
                                    promotion.image.push({ 'image_file': config.web_server.url + '/uploads/' + file.filename });
                                } else if (ext == '.avi' || ext == '.mov' || ext == '.wmv' || ext == '.mp4') {
                                    promotion.video.push({ video_file: config.web_server.url + '/uploads/' + file.filename });
                                }
                            })
                        }
                    }
                    promotion.promotionText = req.body.promotionText || promotion.promotionText;
                    if (req.body.longitude || req.body.latitude) {
                        promotion.promoLocation = { type: "Point", coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.latitude)] };
                    } else {
                        promotion.promoLocation = promotion.promoLocation;
                    }

                    models.Promotion.update({}, promotion, function (err, result) {
                        if (err) {
                            return response.failure(err, 400);
                        } else {
                            return response.data(result, 200);
                        }
                    })
                }
            });
        }
    });
}


//==========================================End updatePromotion============================================//

exports.promotion_search = function (req, res) {
    models.Promotion.find({ promotionText: new RegExp(req.query.searchText, 'i') })
        .sort('created_at')
        .exec(function (err, promotions) {
            var response = response_helper(res);
            if (err) return response.failure(err, 400);
            else if (!promotions) return response.failure(err, 400, 'No promotion found')
            else return response.data(promotions, 200);
        })
};
