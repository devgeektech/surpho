"use strict";
var mongoose = require('mongoose');
const models = require('mongoose').models;
const jwt = require('jsonwebtoken');
const async = require('async');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const FCM = require('fcm-node');
const config = require('../settings/config');
const string = require('../settings/string');
const response_helper = require('../helpers/response');
const authentication = require('../middleware/authentication');
const userLoginAndRegistration = require('../components/authorization');
const user_validate = require('../validators/user');
var FifoArray = require('fifo-array');
var bcrypt = require('bcrypt-nodejs');
const notification = require('../components/notificationLogs');
var androidPush = require('../components/androidNotification');
var iosPush = require('../components/iosNotification');
var concat = require('unique-concat');

var checkVersion = function (body, version, callback) {
    var isNormal, isCritical;
    switch (body.deviceType) {
        case 'ios':
            if (parseFloat(body.iosVersion) >= parseFloat(version.normalIOSVersion)) {
                isNormal = false
            } else {
                isNormal = true;
            }
            if (parseFloat(body.iosVersion) >= parseFloat(version.criticalIOSVersion)) {
                isCritical = false
            } else {
                isCritical = true;
            }
            return callback({ isCritical: isCritical, isNormal: isNormal });
            break;

        case 'android':
            if (parseFloat(body.androidVersion) >= parseFloat(version.normalAndroidVersion)) {
                isNormal = false
            } else isNormal = true;
            if (parseFloat(body.androidVersion) >= parseFloat(version.criticalAndroidVersion)) {
                isCritical = false
            } else isCritical = true;
            return callback({ isCritical: isCritical, isNormal: isNormal });
            break;
        case 'web':
            return callback({ isCritical: false, isNormal: false });
            break;
    };
}


var userSignUpReturnModel = function (user) {
    return {
        _id: user._id,
        name: user.name,
        profilePic: user.profilePic,
        device: user.device,
        email: user.email,
        role: user.role,
        token: user.token,
        online: user.online,
        chatId: user.chatId,
        description: user.description,
        dob: user.dob,
        userName: user.userName,
        phoneNumber: user.phoneNumber,
        viewCounter: user.viewCounter,
        isPhoneVerified: user.isPhoneVerified,
        language: user.language,
        seen: user.seen,
    }
}


var singleProfileReturnModel = function (user, counts) {
    return {
        _id: user._id,
        name: user.name,
        profilePic: user.profilePic,
        email: user.email,
        online: user.online,
        userName: user.userName,
        description: user.description,
        followersCount: counts.followersCount,
        followingCount: counts.followingCount,
        friendsCount: counts.friendsCount,
        viewsCount: counts.viewsCount,
        viewCounter: user.viewCounter,
        isFollower: user._doc.isFollower,
        isFollowing: user._doc.isFollowing,
        isBlocked: user._doc.isBlocked,
        blockedUsersCount: counts.blockedUsersCount,
        isRequested: user._doc.isRequested,
        isPrivate: user.isPrivate,
        isPhoneVerified: user.isPhoneVerified
    }
}

/*=======================register======================*/
/*******************************************************************************
Api for  sign-up.
*******************************************************************************/

exports.signUp = function (req, res) {
    async.waterfall([
        function (cb) {
            user_validate.signUp_form(req.body).is_valid(function (err, form) {
                if (err) return cb(err);
                cb(null, form.data);
            });
        },
        function (form, cb) {
            models.User.findOne({ "phoneNumber.phone": form.phone, isRegistration: false, userName: form.userName }, function (err, user) {
                if (err) return cb(err);
                cb(null, form, user);
            })
        },
        function (form, user, cb) {
            if (user) {
                models.User.find({ userName: form.userName }, function (err, result) {
                    if (err) return cb(err);
                    if (result.length > 0 && result[0]._id.toString() != user._id.toString()) {
                        return cb('username is allready taken');
                    } else {
                        cb(null, form, user);
                    }
                })
            } else {
                models.User.find({ userName: form.userName }, function (err, result) {
                    if (err) return cb(err);
                    if (result.length > 0) {
                        return cb('username is allready taken');
                    } else {
                        cb(null, form, user);
                    }
                })
            }

        },
        function (form, user, cb) {
            if (user) {
                models.User.find({ "phoneNumber.phone": form.phone }, function (err, result) {
                    if (err) return cb(err);
                    if (result.length > 0 && result[0]._id.toString() != user._id.toString()) {
                        return cb('phone is allready taken');
                    } else {
                        cb(null, form, user);
                    }
                })
            } else {
                models.User.find({ "phoneNumber.phone": form.phone }, function (err, result) {
                    if (err) return cb(err);
                    if (result.length > 0) {
                        return cb('phone is allready taken');
                    } else {
                        cb(null, form, user);
                    }
                })
            }

        },
        function (form, user, cb) {
            if (user) {
                models.User.find({ email: req.body.email }, function (err, usr) {
                    if (err) return cb(err);
                    if (usr.length > 0 && result[0]._id.toString() != user._id.toString()) {
                        return cb('Email is allready taken');
                    } else {
                        cb(null, form, user);
                    }
                })
            } else {
                models.User.find({ email: req.body.email }, function (err, usr) {
                    if (err) return cb(err);
                    if (usr.length > 0) {
                        return cb('Email is allready taken');
                    } else {
                        cb(null, form, user);
                    }
                })
            }

        },
        function (form, user, cb) {
            if (form.password == form.confirmPassword) {
                cb(null, form, user);
            } else {
                return cb('Confirm password does not match with password');
            }
        },

        function (form, result, cb) {
            models.Version.findOne()
                .select('normalAndroidVersion criticalAndroidVersion normalIOSVersion criticalIOSVersion')
                .exec(function (err, version) {
                    if (err) return cb(err);
                    if (!version) return cb('versions not found in DB')

                    if (form.deviceType == 'android') {
                        form.androidVersion = form.version;
                    } else if (form.deviceType == 'ios') {
                        form.iosVersion = form.version;
                    }
                    checkVersion(form, version, function (versionResult) {
                        console.log(versionResult, 'ttttt')
                        if (versionResult.isCritical) {
                            return cb(versionResult);
                        } else cb(null, form, result, versionResult)
                    })
                });
        },
        function (form, user, versionResult, cb) {
            if (req.files.length > 0) {
                var photo = req.files[0].filename;
            }
            if (!user) {
                var user = new models.User();
                user.device.deviceId = form.deviceId;
                user.device.deviceType = form.deviceType;
                user.phoneNumber.phone = form.phone;
                user.userName = form.userName;
                user.name = form.name;
                user.email = form.email;
                user.phoneNumber.countryCode = req.body.countryCode;
                user.description = req.body.description;
                user.profilePic = "photo";
                user.password = models.User.schema.methods.generateHash(req.body.password);
            };
            user.profilePic = "photo";
            user.email = form.email;
            user.device.deviceId = form.deviceId;
            user.device.deviceType = form.deviceType;
            user.phoneNumber.phone = form.phone;
            user.userName = form.userName;
            user.name = form.name;
            user.profilePic = form.profilePic;
            user.phoneNumber.countryCode = form.countryCode;
            user.description = form.description;
            console.log(user, 'testing1234');
            user.password = models.User.schema.methods.generateHash(req.body.password),
                user.save(function (err, updatedUser) {
                    if (err) return cb(err);
                    cb(null, updatedUser, versionResult);
                })
        },
        function (user, versionResult, cb) {
            var token = authentication.getToken(user);
            var query;
            if (user.isRegistration) {
                query = { $set: { 'token': token } }
            } else {
                query = { $set: { 'temporaryToken': token } }
            }

            models.User.findOneAndUpdate({ _id: user._id }, query, { new: true })
                .exec(function (err, updatedUser) {
                    if (err) return cb(err);
                    if (!updatedUser) return cb('user Not Find for update token');
                    cb(null, versionResult, updatedUser)
                })
        }
    ], function (err, versionResult, user) {
        var response = response_helper(res);
        var data = {
            versionResult: versionResult || err,
            user: user
        };
        if (err && !err.isCritical) return response.failure(err, 400);
        return response.data(data, 200);
    })
}


/*====social=====*/

exports.social = function (req, res) {
    var firstTimeLogin = false;
    async.waterfall([
        function (cb) {
            const email = req.body.socialId + '@' + req.body.loginType + '.com';
            if (!email) return cb('Social id is required.');
            models.User.findOne({ email: email })
                .exec(function (err, User) {
                    if (err) return cb(err);
                    if (User) {
                        if (User.isBlockedByAdmin == true) {
                            var response = response_helper(res);
                            return response.failure("User is Blocked By Admin", 400);
                        } else {
                            cb(null);
                        }
                    } else {
                        cb(null);
                    }

                });
        },
        function (cb) {
            user_validate.social_form(req.body).is_valid(function (err, form) {
                if (err) return cb(err);
                cb(null, form.data);
            });
        },
        function (form, cb) {
            models.Version.findOne()
                .select('normalAndroidVersion criticalAndroidVersion normalIOSVersion criticalIOSVersion')
                .exec(function (err, version) {
                    if (err) return cb(err);
                    if (!version) return cb('versions not found in DB')
                    checkVersion(form, version, function (versionResult) {
                        if (versionResult.isCritical) {
                            return cb(versionResult);
                        } else cb(null, versionResult, form)
                    })
                });
        },
        function (versionResult, form, cb) {
            const email = form.socialId + '@' + form.loginType + '.com';
            if (!email) return cb('Social id is required.')
            models.User.findOne({
                email: email
            })
                .exec(function (err, user) {
                    if (err) return cb(err);
                    if (!user) {
                        firstTimeLogin = true;
                        var user = new models.User();
                        user.email = email;
                        user.social_media_links[form.loginType].id = form.socialId;
                        user.name = "";
                        user.profilePic = form.profilePic;
                    };
                    user.device.deviceId = form.deviceId;
                    user.device.deviceType = form.deviceType;
                    user.language = "English";
                    user.token = authentication.getToken(user);
                    user.status = true;
                    user.seen = true;
                    user.save(function (err, updatedUser) {
                        if (err) return cb(err);
                        cb(null, versionResult, firstTimeLogin, userSignUpReturnModel(user));
                    })
                })
        }
    ], function (err, versionResult, firstTimeLogin, user) {
        var response = response_helper(res);
        var data = {
            firstTimeLogin: firstTimeLogin,
            versionResult: versionResult || err,
            user: user
        };
        if (err && !err.isCritical) return response.failure(err, 400);
        return response.data(data, 200);
    })
}


exports.refreshToken = function (req, res) {
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.user.id
            }, function (err, user) {
                if (err) return cb(err);
                if (!user) return cb('user does not exist');
                cb(null, user);
            });
        },
        function (user, cb) {
            models.Version.findOne()
                .select('normalAndroidVersion criticalAndroidVersion normalIOSVersion criticalIOSVersion')
                .exec(function (err, version) {
                    if (err) return cb(err);
                    if (!version) return cb('versions not found in DB')

                    checkVersion(req.body, version, function (versionResult) {
                        if (versionResult.isCritical) {
                            return cb(versionResult);
                        } else cb(null, user, versionResult)
                    })
                });
        },
        function (user, versionResult, cb) {
            user.device.deviceId = req.body.deviceId;
            user.device.deviceType = req.body.deviceType;
            user.language = req.body.language || user.language;
            user.token = authentication.getToken(user);
            user.save(function (err, userUpdated) {
                if (err) return cb(err);
                cb(null, userUpdated, versionResult);
            })
        }
    ], function (err, user, versionResult) {
        var response = response_helper(res);
        var data = {
            versionResult: versionResult || err,
            user: user
        };
        if (err && !err.isCritical) return response.failure(err, 400);
        return response.data(data, 200);
    })
}

exports.getRecentUserList = function (req, res) {
    var results = [];
    var query = {
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 10,
    }
    async.waterfall([
        function (cb) {
            models.User.findOne({ _id: req.user.id })
                .exec(function (err, user) {
                    if (err) return cb(err);
                    if (!user) return cb('user does not exist')
                    cb(null, user);
                });
        },
        function (user, cb) {
            models.User.find({
                _id: { $in: user.recentUsers },
                $or: [{ status: true },
                { isBlockedByAdmin: false }]
            })
                .limit(query.limit)
                .skip(query.skip)
                .select('_id name  email userName profilePic')

                .exec(function (err, users) {
                    if (err) return cb(err);
                    if (!users) return cb('user does not exist');
                    cb(null, users, user);
                });
        },

    ], function (err, users, user) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        var count = user.recentUsers.length;
        _.each(user.recentUsers, function (val) {
            if (val) {
                _.each(users, function (user) {
                    if (user._id == val.toString()) {
                        results.push(user)
                        return false;
                    }
                });
            }
        });
        return response.page(results, 200, count, query.skip);
    })
}


exports.updateUser = function (req, res) {
    if (req.body.password) {
        const password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    }
    async.waterfall([
        function (cb) {
            if (req.body.userName && req.body.userName != '') {
                cb(null);
            } else {
                return cb('Username is mandatory');
            }
        },
        function (cb) {
            models.User.find({ userName: req.body.userName })
                .exec(function (err, user) {
                    if (err) return cb(err);
                    if (user.length > 0 && user[0].id != req.user.id) return cb("Username already exists");
                    cb(null, user);
                })
        },
        function (user, cb) {
            const password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
            cb(null, password, user);
        },
        function (password, userData, cb) {
            models.User.findOne({
                _id: req.user.id
            }, function (err, user) {
                if (err) return cb(err);
                if (!user) return cb('user not exist');
                user.description = req.body.description || user.description;
                user.userName = req.body.userName || user.userName;
                user.password = password || user.password;
                user.name = req.body.name || user.name;
                user.email = req.body.email || user.email;
                user.language = req.body.language || user.language;
                user.profilePic = req.body.profilePic || user.profilePic;
                user.save((err, done) => {
                    if (err) return cb(err);
                    cb(null, userSignUpReturnModel(user));
                })
            });
        }
    ], function (err, user) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data({ 'user': user }, 200);
    });
}



var addFriend = function (profile, targetProfile, callback) {
    models.User.findOneAndUpdate({
        _id: profile.id
    }, {
            $addToSet: {
                friends: targetProfile.id
            }
        }, { new: true }, function (err, updatedMyProfile) {
            if (err) return callback(err);
            if (!updatedMyProfile) return callback('updatedMyProfile does not exist');
            callback(null, updatedMyProfile);
        });
}


var addFollower = function (profile, targetProfile, callback) {
    models.User.findOneAndUpdate({
        _id: targetProfile.id
    }, {
            $addToSet: {
                followers: profile.id
            }
        }, { new: true }, function (err, updatedTargetProfile) {
            if (err) return callback(err);
            if (!updatedTargetProfile) return callback('updatedMyProfile does not exist');
            callback(null, updatedTargetProfile)
        });
}

var addFollowing = function (profile, targetProfile, callback) {
    models.User.findOneAndUpdate({
        _id: profile.id
    }, {
            $addToSet: {
                following: targetProfile.id
            }
        }, { new: true }, function (err, updatedMyProfile) {
            if (err) return callback(err);
            if (!updatedMyProfile) return callback('updatedTargetProfile does not exist');
            callback(null, updatedMyProfile)
        });
}


exports.follow = function (req, res) {
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.user.id,
                status: true
            })
                .exec(function (err, myProfileData) {
                    if (err) return cb(err);
                    if (!myProfileData) return cb('token profile does not exist');
                    if (myProfileData._id.toString() == req.params.id) return cb("User can't Follow himself");
                    cb(null)
                })
        },
        function (cb) {
            models.User.findOne({
                _id: req.user.id,
                status: true
            })
                .exec(function (err, myProfile) {
                    if (err) return cb(err);
                    if (!myProfile) return cb('token profile does not exist');
                    cb(null, myProfile)
                })
        },
        function (myProfile, cb) {
            models.User.findOne({
                _id: req.params.id,
                status: true
            })
                .exec(function (err, targetProfile) {
                    if (err) return cb(err);
                    if (!targetProfile) return cb('targetProfile does not exist');
                    cb(null, myProfile, targetProfile)
                });
        },
        function (myProfile, targetProfile, cb) {
            var related = {
                isRequested: myProfile.addFollowRequestFlag(targetProfile, req.user.id),
            };
            if (related.isRequested) {
                return cb('already requested');
            } else {
                cb(null, myProfile, targetProfile)
            }
        },
        function (myProfile, targetProfile, cb) {
            if (targetProfile.isPrivate) {
                models.User.update({
                    _id: req.params.id
                },
                    {
                        $addToSet: {
                            pendingRequests: req.user.id,
                        },
                    },
                    {
                        new: true
                    })
                    .exec(function (err, user) {
                        if (err) return cb(err);
                        var message = myProfile.name + ' has requested to pursue you.';
                        var title = "Pursue Request";
                        var type = "followRequest";

                        var data = {};
                        data.message = message;
                        data.title = title;
                        data.type = type;
                        data.actionId = req.user.id;
                        notification.create(req.params.id, req.user.id, null, type, title, message, (err, response) => {
                            if (err) {
                                return cb("Error in inserting notifcation logs.");
                            } else {
                                if (targetProfile.device.deviceType == "android") {
                                    androidPush.androidPushNotification(targetProfile.device.deviceId, data, (err) => {
                                    })
                                } else if (targetProfile.device.deviceType == "ios") {
                                    iosPush.apn(targetProfile.device.deviceId, data, (err) => {
                                    })
                                } else {
                                    console.log("No valid device type.")
                                }
                                return cb("privateProfileCase")
                            }
                        })
                    })
            } else {
                cb(null, myProfile, targetProfile);
            }
        },
        function (myProfile, targetProfile, cb) {
            var related = {
                isFollower: myProfile.addFollowersFlag(myProfile, req.params.id),
                isFollowing: myProfile.addFollowingFlag(myProfile, req.params.id)
            };

            if (related.isFollowing) {
                return cb('already pursuing');
            } else {
                cb(null, related, myProfile, targetProfile)
            }
        },
        function (related, myProfile, targetProfile, cb) {
            if (related.isFollower) {
                addFriend(myProfile, targetProfile, function (err, updatedMyUser) {
                    if (err) return cb(err);
                    addFriend(targetProfile, myProfile, function (err, updatedTargetUser) {
                        if (err) return cb(err);
                        cb(null, related, updatedMyUser, updatedTargetUser)
                    });
                });
            } else cb(null, related, myProfile, targetProfile)
        },
        function (related, myProfile, targetProfile, cb) {
            addFollower(myProfile, targetProfile, function (err, updatedTargetUser) {
                if (err) return cb(err);
                addFollowing(myProfile, targetProfile, function (err, updatedMyUser) {
                    if (err) return cb(err);
                    related.isFriend = myProfile.addFriendsFlag(myProfile, req.params.id)
                    cb(null, related, updatedMyUser, updatedTargetUser)
                });
            });
        },
        function (related, updatedMyUser, updatedTargetUser, cb) {
            var message, title, type;
            if (related.isFollower) {
                var messageFriends = 'Congratulations! You are now friends with ' + updatedMyUser.name;
                var titleFriends = "Friends";
                var typeFriends = "friends";
                message = 'You have been pursued by ' + updatedMyUser.name;
                if (updatedTargetUser.isPrivate) {
                    title = "Pursue Request";
                } else {
                    title = "Pursue";
                }

                type = "follow";

                var data = {};
                data.message = message;
                data.title = title;
                data.type = type;
                data.actionId = req.user.id;

                var dataFriends = {};
                dataFriends.message = messageFriends;
                dataFriends.title = titleFriends;
                dataFriends.type = typeFriends;
                dataFriends.actionId = req.user.id
                notification.create(req.params.id, req.user.id, null, typeFriends, titleFriends, messageFriends, (err, response) => {
                    if (err) {
                        return cb("Error in inserting notifcation logs.");
                    } else {
                        if (updatedTargetUser.device.deviceType == "android") {
                            androidPush.androidPushNotification(updatedTargetUser.device.deviceId, dataFriends, (err) => {

                            })
                        } else if (updatedTargetUser.device.deviceType == "ios") {
                            iosPush.apn(updatedTargetUser.device.deviceId, data, (err) => {

                            })
                        } else {
                            console.log("No valid device type.")
                        }
                        cb(null, related, updatedMyUser, updatedTargetUser);
                    }
                })
                notification.create(req.params.id, req.user.id, null, type, title, message, (err, response) => {
                    if (err) {
                        return cb("Error in inserting notifcation logs.");
                    } else {
                        if (updatedTargetUser.device.deviceType == "android") {
                            androidPush.androidPushNotification(updatedTargetUser.device.deviceId, data, (err) => {

                            })
                        } else if (updatedTargetUser.device.deviceType == "ios") {
                            iosPush.apn(updatedTargetUser.device.deviceId, data, (err) => {

                            })
                        } else {
                            console.log("No valid device type.")
                        }
                        cb(null, related, updatedMyUser, updatedTargetUser);
                    }
                })
            } else {
                message = 'You have been pursued by ' + updatedMyUser.name;
                title = "Pursue";
                type = "follow";
                var data = {};
                data.message = message;
                data.title = title;
                data.type = type;
                data.actionId = req.user.id;
                notification.create(req.params.id, req.user.id, null, type, title, message, (err, response) => {
                    if (err) {
                        return cb("Error in inserting notifcation logs.");
                    } else {
                        if (updatedTargetUser.device.deviceType == "android") {
                            androidPush.androidPushNotification(updatedTargetUser.device.deviceId, data, (err) => {
                            })
                        } else if (updatedTargetUser.device.deviceType == "ios") {
                            iosPush.apn(updatedTargetUser.device.deviceId, data, (err) => {
                            })
                        } else {
                            console.log("No valid device type.")
                        }
                        cb(null, related, updatedMyUser, updatedTargetUser);
                    }
                })
            }
        },
    ], function (err) {
        var response = response_helper(res);
        if (err && err != 'privateProfileCase') return response.failure(err, 400)
        if (err == 'privateProfileCase') {
            var message = "pursue request Sent"
        } else var message = "user pursued";
        return response.success(message, 200);
    })
}



var profileViews;
exports.getList = function (req, res) {
    var result = [];
    var query = {
        type: req.query.type || 'followers',
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 10,
        name: req.query.name || "",
    }
    var t = query.type;
    console.log(t);
    var tokenRes = [];
    var tokenFollowRes = [];
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.user.id,  /* req.params.id */
            })
                .exec(function (err, tokenData) {
                    if (err) return cb(err);
                    if (!tokenData) return cb('user does not exist');
                    tokenRes = tokenData.following;
                    tokenFollowRes = tokenData.followers;
                    cb(null);
                });
        },
        function (cb) {
            models.User.findOne({
                _id: req.user.id,  /* req.params.id */
                status: true,
                isBlockedByAdmin: false
            })
                .exec(function (err, myProfile) {
                    if (err) return cb(err);
                    if (!myProfile) return cb('user does not exist')
                    cb(null, myProfile);
                });
        },
        function (myProfile, cb) {

            var criteria = {};

            var blockUserList = myProfile.blockedBy.concat(myProfile.blockedUsers);

            if (query.name) {
                criteria = {
                    $and: [
                        { _id: { $nin: blockUserList } },
                        {
                            $or: [
                                { name: { $regex: query.name } },
                                { userName: { $regex: query.name } }
                            ]
                        }
                    ]
                }
            }
            if (query.type == "friends") {
                criteria = { '_id': { $nin: blockUserList } };
            }
            var mongoQuery = models.User.find(criteria)
            var countQuery = models.User.count(criteria)

            mongoQuery.skip(query.skip)
            mongoQuery.limit(query.limit)
            mongoQuery.select('name profilePic description userName followers friends following  blockedUsers pendingRequests isPrivate isPhoneVerified')
            mongoQuery.where("_id").ne(req.user.id)
            mongoQuery.where("status").equals(true)
            mongoQuery.where('isBlockedByAdmin').equals(false)


            countQuery.skip(query.skip)
            countQuery.limit(query.limit)
            countQuery.select('name profilePic description userName blockedUsers followers friends following pendingRequests isPrivate isPhoneVerified')
            countQuery.where("_id").ne(req.user.id)

            if (query.type == "followers") {
                var followersList = myProfile.followers;

                followersList = followersList.filter(function (val) {
                    if (myProfile.blockedBy.indexOf(val) === -1 && myProfile.blockedUsers.indexOf(val) === -1) {
                        return val;
                    }
                });
                mongoQuery.where("_id").in(followersList)
                countQuery.where("_id").in(followersList)
            }
            if (query.type == "following") {
                var followingList = myProfile.following;

                followingList = followingList.filter(function (val) {
                    if (myProfile.blockedBy.indexOf(val) === -1 && myProfile.blockedUsers.indexOf(val) === -1) {
                        return val;
                    }
                });
                mongoQuery.where("_id").in(followingList)
                countQuery.where("_id").in(followingList)
            }
            if (query.type == "block") {
                var idArray = [];
                var idArray = myProfile.blockedUsers.concat(myProfile.blockedBy);
                mongoQuery.where("_id").in(idArray)
                countQuery.where("_id").in(idArray)
            }
            if (query.type == "relatedUsers") {
                var relatedUsers = myProfile.following.concat(myProfile.followers);
                mongoQuery.where("_id").in(relatedUsers)
                countQuery.where("_id").in(relatedUsers)
                mongoQuery.where('_id').nin(myProfile.blockedUsers)
                mongoQuery.where('_id').nin(myProfile.blockedBy)
                countQuery.where("_id").in(myProfile.blockedUsers)
                countQuery.where("_id").in(myProfile.blockedBy)
            }
            if (query.type == "friends") {
                var criteria = { '_id': { $nin: blockUserList } };
                var friendList = myProfile.friends;

                friendList = friendList.filter(function (val) {
                    if (myProfile.blockedBy.indexOf(val) === -1 && myProfile.blockedUsers.indexOf(val) === -1) {
                        return val;
                    }
                });

                mongoQuery.where("_id").in(friendList)
                countQuery.where("_id").in(friendList)
            }
            if (query.type == "views") {
                profileViews = myProfile.views;
                mongoQuery.where("_id").in(myProfile.views)
                countQuery.where("_id").in(myProfile.views)
                mongoQuery.where('_id').nin(myProfile.blockedUsers)
                mongoQuery.where('_id').nin(myProfile.blockedBy)
                countQuery.where("_id").in(myProfile.blockedUsers)
                countQuery.where("_id").in(myProfile.blockedBy)
            }
            if (query.type == "blockedUsers") {
                mongoQuery.where("_id").in(myProfile.blockedUsers)
                countQuery.where("_id").in(myProfile.blockedUsers)
            }
            if (query.type == "pendingRequests") {
                mongoQuery.where("_id").in(myProfile.pendingRequests)
                countQuery.where("_id").in(myProfile.pendingRequests)
                mongoQuery.where('_id').nin(myProfile.blockedUsers)
                mongoQuery.where('_id').nin(myProfile.blockedBy)
                countQuery.where("_id").in(myProfile.blockedUsers)
                countQuery.where("_id").in(myProfile.blockedBy)
            }
            if (query.type == 'users') {
                mongoQuery.where('_id').nin(myProfile.blockedUsers)
                mongoQuery.where('_id').nin(myProfile.blockedBy)
                countQuery.where("_id").nin(myProfile.blockedUsers)
                countQuery.where("_id").nin(myProfile.blockedBy)
            }

            mongoQuery.exec(function (err, users) {
                if (err) return cb(err);
                cb(null, users, countQuery);
            });
        },
        function (users, countQuery, cb) {
            async.eachSeries(users, function (singleUser, next) {
                singleUser._doc.isFollower = singleUser.otherFollowers(singleUser, tokenFollowRes);
                singleUser._doc.isFollowing = singleUser.otherFollowing(singleUser, tokenRes);
                singleUser._doc.isFriend = singleUser.addFriendsFlag(singleUser, req.user.id);
                singleUser._doc.isBlocked = singleUser.addBlockedUserFlag(singleUser, req.user.id);
                singleUser._doc.isRequested = singleUser.addFollowRequestFlag(singleUser, req.user.id);
                next()
            }, function (err) {
                if (err) return cb(err);
                cb(null, users, countQuery)
            });
        },
        function (users, countQuery, cb) {
            models.User.findOne({ _id: req.user.id })
                .exec(function (err, userData) {
                    console.log("userData wala data", userData[t]);
                    var lengthCount = userData[query.type].length;
                    cb(null, users, lengthCount);
                })
            // countQuery
            //     .exec(function (err, count) {
            //         cb(null, users, count);
            //     });
        }
    ], function (err, users, count) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        if (query.type == "views") {
            _.each(profileViews, function (val) {
                if (val) {
                    _.each(users, function (user) {
                        if (user._id == val.toString()) {
                            result.push(user)
                            return false;
                        }
                    });
                }
            });
            return response.page(result, 200, count, query.skip);
        } else {
            return response.page(users, 200, count, query.skip);
        }
    })
}

exports.otherUserFollowerList = function (req, res) {
    var dataToken = [];
    var ParamsUser = [];
    var tokenFollowRes = [];
    var dataToken;
    var tokenId;
    models.User.findOne({
        _id: req.user.id
    })
        .exec(function (err, tokenData) {
            if (err) return response.failure(err, 400);
            if (!tokenData) return response.failure("User Not Fount", 400);
            tokenId = tokenData.following;
            tokenFollowRes = tokenData.followers;
            dataToken = tokenData;
        })
    models.User.findOne({
        _id: req.params.id
    })
        .exec(function (err, ParamsData) {
            if (err) return response.failure(err, 400);;
            if (!ParamsData) return response.failure("User Not Fount", 400);
            ParamsUser = ParamsData;
        })
    var query = {
        type: req.query.type || 'users',
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 10,
        name: req.query.name || "",
    }
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.params.id

            })
                .exec(function (err, myProfile) {
                    if (err) return cb(err);
                    if (!myProfile) return cb('user does not exist')
                    cb(null, myProfile);
                });
        },
        function (myProfile, cb) {
            var criteria = {};
            var blockUserList = myProfile.blockedBy.concat(myProfile.blockedUsers);
            if (query.name) {
                criteria = {
                    '_id': { $nin: blockUserList },
                    $or: [
                        { name: { $regex: query.name } },
                        { userName: { $regex: query.name } }
                    ]
                }
            }
            if (query.type == "friends") {
                criteria = { '_id': { $nin: blockUserList } };
            }
            var mongoQuery = models.User.find(criteria)
            var countQuery = models.User.count(criteria)

            mongoQuery.skip(query.skip)
            mongoQuery.limit(query.limit)
            mongoQuery.select('name profilePic description userName followers friends following  blockedUsers pendingRequests isPrivate isPhoneVerified')
            mongoQuery.where("status").equals(true)
            mongoQuery.where('isBlockedByAdmin').equals(false)

            countQuery.skip(query.skip)
            countQuery.limit(query.limit)
            countQuery.select('name profilePic description userName blockedUsers followers friends following pendingRequests isPrivate isPhoneVerified')
            countQuery.where("status").equals(true)
            countQuery.where('isBlockedByAdmin').equals(false)

            if (query.type == "followers") {
                mongoQuery.where("_id").in(myProfile.followers)
                countQuery.where("_id").in(myProfile.followers)
                mongoQuery.where('_id').nin(myProfile.blockedUsers)
                mongoQuery.where('_id').nin(myProfile.blockedBy)
                countQuery.where("_id").in(myProfile.blockedUsers)
                countQuery.where("_id").in(myProfile.blockedBy)
            }
            if (query.type == "following") {
                mongoQuery.where("_id").in(myProfile.following)
                countQuery.where("_id").in(myProfile.following)
            }
            if (query.type == "block") {
                mongoQuery.where("_id").in(myProfile.blockedUsers)
                mongoQuery.where("_id").in(myProfile.blockedBy)
                countQuery.where("_id").in(myProfile.blockedUsers)
                countQuery.where("_id").in(myProfile.blockedBy)
            }
            if (query.type == "relatedUsers") {
                var relatedUsers = myProfile.following.concat(myProfile.followers);
                mongoQuery.where("_id").in(relatedUsers)
                countQuery.where("_id").in(relatedUsers)
                mongoQuery.where('_id').nin(myProfile.blockedUsers)
                mongoQuery.where('_id').nin(myProfile.blockedBy)
                countQuery.where("_id").in(myProfile.blockedUsers)
                countQuery.where("_id").in(myProfile.blockedBy)
            }
            if (query.type == "friends") {
                mongoQuery.where("_id").in(myProfile.friends)
                countQuery.where("_id").in(myProfile.friends)
                mongoQuery.where('_id').nin(myProfile.blockedUsers)
                mongoQuery.where('_id').nin(myProfile.blockedBy)
            }
            if (query.type == "views") {
                mongoQuery.where("_id").in(myProfile.views)
                countQuery.where("_id").in(myProfile.views)
                mongoQuery.where('_id').nin(myProfile.blockedUsers)
                mongoQuery.where('_id').nin(myProfile.blockedBy)
                countQuery.where("_id").in(myProfile.blockedUsers)
                countQuery.where("_id").in(myProfile.blockedBy)
            }
            if (query.type == "blockedUsers") {
                mongoQuery.where("_id").in(myProfile.blockedUsers)
                mongoQuery.where("_id").in(myProfile.blockedBy)
                countQuery.where("_id").in(myProfile.blockedUsers)
                countQuery.where("_id").in(myProfile.blockedBy)
                countQuery.where("_id").in(myProfile.blockedUsers)
                countQuery.where("_id").in(myProfile.blockedBy)

            }
            if (query.type == "pendingRequests") {
                mongoQuery.where("_id").in(myProfile.pendingRequests)
                countQuery.where("_id").in(myProfile.pendingRequests)
                mongoQuery.where('_id').nin(myProfile.blockedUsers)
                mongoQuery.where('_id').nin(myProfile.blockedBy)
                countQuery.where("_id").in(myProfile.blockedUsers)
                countQuery.where("_id").in(myProfile.blockedBy)
            }
            if (query.type === 'users') {
                mongoQuery.where('_id').nin(myProfile.blockedUsers)
                mongoQuery.where('_id').nin(myProfile.blockedBy)
                countQuery.where("_id").nin(myProfile.blockedUsers)
                countQuery.where("_id").nin(myProfile.blockedBy)
            }

            mongoQuery
                .exec(function (err, users) {
                    if (err) return cb(err);
                    cb(null, users, countQuery);
                });
        },
        function (users, countQuery, cb) {
            async.eachSeries(users, function (singleUser, next) {
                singleUser._doc.isFollower = singleUser.otherFollowers(singleUser, tokenFollowRes);
                singleUser._doc.isFollowing = singleUser.otherFollowingOther(singleUser, dataToken);
                singleUser._doc.isFriend = singleUser.addFriendsFlagOther(singleUser, dataToken);
                singleUser._doc.isBlocked = singleUser.addBlockedUserOther(singleUser, dataToken);
                singleUser._doc.isRequested = singleUser.addFollowRequestOther(singleUser, dataToken);
                next()
            }, function (err) {
                if (err) return cb(err);
                cb(null, users, countQuery)
            });
        },
        function (users, countQuery, cb) {
            countQuery
                .exec(function (err, count) {
                    cb(null, users, count);
                });
        }
    ], function (err, users, count) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.page(users, 200, count, query.skip);
    })
}



exports.unfollow = function (req, res) {
    async.waterfall([
        function (cb) {
            models.User.findOneAndUpdate({
                _id: req.params.id
            }, {
                    $pull: {
                        followers: req.user.id,
                        friends: req.user.id,
                        pendingRequests: req.user.id
                    }
                }, { new: true }, function (err, user) {
                    if (err) return cb(err);
                    cb(null);
                })
        },
        function (cb) {
            models.User.findOneAndUpdate({
                _id: req.user.id
            }, {
                    $pull: {
                        following: req.params.id,
                        friends: req.params.id
                    }
                }, { new: true }, function (err, user) {
                    if (err) return cb(err);
                    cb(null, user);
                })
        }
    ], function (err) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.success("unPursue successful", 200);
    })
}

exports.getProfile = function (req, res) {
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.params.id
            })
                .exec(function (err, user) {
                    if (err) return cb(err);
                    if (!user) return cb('User does not exist', 400);
                    if (user.isBlockedByAdmin == true) {
                        return cb('User does not exist', 400);
                    }
                    cb(null)
                })
        },
        function (cb) {
            models.User.findOne({
                _id: req.params.id
            })
                .select('name seen email status blockedBy blockedUsers pendingRequests description profilePic followers viewCounter following friends online views isPrivate isPhoneVerified userName')
                .exec(function (err, user) {
                    if (err) return cb(err);
                    var isBlocked = user.addBlockedUserFlag(user, req.user.id);
                    if (!user || !user.status || isBlocked) return cb('user does not exist');
                    cb(null, user)
                })
        },
        function (user, cb) {
            models.User.find({
                status: false,
                isBlockedByAdmin: false
            })
                .select('_id name')
                .exec(function (err, deactivatedUsers) {
                    if (err) return cb(err);
                    cb(null, user, deactivatedUsers)
                })
        },
        function (user, deactivatedUsers, cb) {
            var blockData = user.blockedBy;
            async.eachSeries(blockData, (i, seriesCallback) => {
                if (i == req.user.id || i == req.params.id) {
                    return cb("User is blocked");
                } else {
                    seriesCallback();
                }
            })
            cb(null, user, deactivatedUsers)
        },
        function (user, deactivatedUsers, cb) {
            models.User.findOne({
                _id: req.user.id
            })

                .select('name email seen status blockedUsers pendingRequests description profilePic followers viewCounter following friends online views isPrivate isPhoneVerified userName')
                .exec(function (err, myProfile) {
                    if (err) return cb(err);
                    if (!myProfile || !myProfile.status) return cb('user does not exist');
                    var isBlocked = user.addBlockedUserFlag(myProfile, req.params.id);
                    cb(null, user, myProfile, deactivatedUsers);
                })
        },
        function (user, myProfile, deactivatedUsers, cb) {
            if (!user.seen || !myProfile.seen) {
                cb(null, user, myProfile, deactivatedUsers);
            } else {
                if (req.params.id != req.user.id) {
                    var fifoArray = new FifoArray(10, user._doc.views);
                    var ifExist = fifoArray.filter(function (item, index) {
                        return item.toString() == req.user.id
                    });
                    if (!ifExist.length) {
                        fifoArray.unshift(req.user.id);
                    } else {
                        var deletedElement = _.remove(fifoArray, function (arrElement) {
                            return arrElement == req.user.id;
                        });
                        if (deletedElement) {
                            fifoArray.unshift(req.user.id);
                        }
                    }
                    user.views = fifoArray;
                    user.viewCounter += 1;
                    user.save(function (err, updatedUser) {
                        if (err) return cb(err);
                        cb(null, updatedUser, myProfile, deactivatedUsers);
                    });
                } else {
                    cb(null, user, myProfile, deactivatedUsers);
                }
            }
        },
        function (user, myProfile, deactivatedUsers, cb) {
            var related = {
                isFollower: user.addFollowersFlag(myProfile, req.params.id),
                isFollowing: user.addFollowingFlag(myProfile, req.params.id),
                isFriend: user.addFriendsFlag(myProfile, req.params.id),
                isBlocked: user.addBlockedUserFlag(myProfile, req.params.id),
                isRequested: user.addFollowRequestFlag(user, req.user.id),
            };
            user._doc.isFollower = related.isFollower;
            user._doc.isFollowing = related.isFollowing;
            user._doc.isFriend = related.isFriend;
            user._doc.isBlocked = related.isBlocked;
            user._doc.isRequested = related.isRequested;
            cb(null, user, deactivatedUsers);
        },
        function (user, deactivatedUsers, cb) {
            var counts = user.addCounts(user, deactivatedUsers);
            user._doc.followersCount = counts.followersCount;
            user._doc.followingCount = counts.followingCount;
            user._doc.friendsCount = counts.friendsCount;
            user._doc.viewsCount = counts.viewsCount;
            user._doc.blockedUsersCount = counts.blockedUsersCount;

            var returnedUser = singleProfileReturnModel(user, counts);
            cb(null, returnedUser, user);
        },
        function (user, seenUser, cb) {
            models.Post.find({ createdBy: req.params.id, isBlocked: false })
                .where('status').equals(true)
                .exec(function (err, posts) {
                    user.PostCountByUser = posts.length;
                    user.seen = seenUser.seen;
                    if (err) return cb(err);
                    cb(null, user)
                })
        }
    ], function (err, user) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(user, 200);
    });
}

exports.delete = function (req, res) {
    async.waterfall([
        function (cb) {
            models.User.findOneAndUpdate({
                _id: req.user.id
            }, {
                    $set: {
                        status: false
                    }
                }, {
                    new: true
                })
                .exec(function (err, user) {
                    if (err) return cb(err);
                    if (!user) return cb('user does not exist');
                    cb(null, user);
                })
        },
    ], function (err, user) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.success('user deleted successfully', 200);
    })
}




/*
 * CHECK PHONE EXISTENCE
 */

exports.checkPhoneExistence = function (req, res) {

    var isExisted = false;
    async.waterfall([
        function (cb) {
            models.User.find({
                'phoneNumber.phone': req.body.phone,
                'phoneNumber.countryCode': req.body.countryCode
            })
                .select('name email phoneNumber description profilePic followers viewCounter following friends online views')
                .exec(function (err, user) {
                    if (err) return cb(err);
                    if (user.length <= 0) {
                        isExisted = false;
                    } else {
                        isExisted = true;
                    }
                    cb(null, isExisted);
                })
        },
    ], function (err, key) {
        var data = {};
        var message;
        data.isExisted = key;
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        if (data.isExisted == true) {
            message = "The phone number you entered already exists."
            return response.failure(message, 400);
        } else {
            return response.data(data, 200)
        }
    })
}

exports.updateUserPhone = function (req, res) {
    let phoneDetail = {
        phone: req.body.phone,
        countryCode: req.body.countryCode
    }
    async.waterfall([
        function (cb) {
            models.User.findOne({
                _id: req.user.id
            }, function (err, user) {
                if (err) return cb(err);
                if (!user) return cb('user not exist');
                user.phoneNumber = phoneDetail;
                user.isPhoneVerified = true;
                user.firebaseId = req.body.firebaseId || user.firebaseId;
                user.countryCodeAndNumber = req.body.countryCode + req.body.phone;
                user.save((err, done) => {
                    if (err) return cb(err);
                    cb(null, user);
                })
            })
        }
    ], function (err, user) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.success("user phone verified", 200);
    });
}

/* 
 * GET FOLLOW USERS
 */

exports.getFollowUsers = function (req, res) {
    var query = {
        limit: req.query.limit || 10,
        skip: req.query.skip || 0
    }
    var finalUsers = [];
    async.waterfall([
        function (cb) {
            models.User.find({
                _id: req.user.id
            })
                .limit(query.limit)
                .skip(query.skip)
                .exec(function (err, user) {
                    if (err) return cb(err);
                    if (!user) return cb('User not found.');
                    cb(null, user[0]);
                })
        },
        function (user, cb) {
            var uniqueUsers = concat(user.followers, user.following);
            var finalResult = {};
            for (var i = 0; i < uniqueUsers.length; ++i)
                finalResult[i] = uniqueUsers[i];
            var count = uniqueUsers.length;
            cb(null, uniqueUsers, count);
        },
        function (uniqueUsers, count, cb) {
            async.eachSeries(uniqueUsers, (i, seriesCallback) => {
                var criteria = {};
                criteria = {
                    _id: i
                }
                var mongoQuery = models.User.find(criteria)

                mongoQuery.skip(query.skip)
                mongoQuery.limit(query.limit)
                mongoQuery.select('name profilePic userName')
                mongoQuery.where("status").equals(true)
                mongoQuery.where('isBlockedByAdmin').equals(false)
                mongoQuery
                    .exec(function (err, users) {
                        if (err) return cb(err);
                        finalUsers.push(users[0]);
                        seriesCallback();
                    });
            }, function (err, resp) {
                cb(null, finalUsers, count)
            })
        },
        function (users, count, cb) {
            async.eachSeries(users, function (singleUser, next) {
                singleUser._doc.isFollower = singleUser.addFollowingFlag(singleUser, req.user.id);
                singleUser._doc.isFollowing = singleUser.addFollowersFlag(singleUser, req.user.id);
                singleUser._doc.isFriend = singleUser.addFriendsFlag(singleUser, req.user.id);
                next()
            }, function (err) {
                if (err) return cb(err);
                cb(null, users, count)
            });
        },
    ], function (err, user, count) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.page(user, 200, count, query.skip);
    })
}


exports.blockUser = function (req, res) {
    var isPresent = false;
    var blockStatus = true;
    if (req.body.blockStatus) {
        blockStatus = true
    } else {
        blockStatus = false
    }
    async.waterfall([
        function (cb) {
            if (req.params.id.toString() == req.user.id.toString()) {
                return cb('user cannot block himself');
            } else {
                cb(null);
            }
        },
        function (cb) {
            models.User.findOne({
                _id: req.user.id
            })
                .exec(function (err, myProfile) {
                    if (err) return cb(err);
                    if (!myProfile) return cb('token profile does not exist');
                    cb(null, myProfile)
                })
        },
        function (myProfile, cb) {
            models.User.findOne({
                _id: req.params.id
            })
                .exec(function (err, targetProfile) {
                    if (err) return cb(err);
                    if (!targetProfile) return cb('targetProfile does not exist');
                    cb(null, myProfile, targetProfile)
                });
        },
        function (myProfile, targetProfile, cb) {
            if (blockStatus == false) {
                models.User.update({
                    _id: req.params.id,
                }, {
                        $pull: {
                            blockedBy: req.user.id,
                        }
                    }, {
                        new: true,
                    })
                    .exec(function (err, user) {
                        if (err) return cb(err);
                        if (!user) return cb('user does not exist');
                        cb(null, myProfile, targetProfile);
                    })
            } else {
                models.User.update({
                    _id: req.params.id,
                }, {
                        $addToSet: {
                            blockedBy: req.user.id,
                        }, $pull: {
                            following: req.user.id,
                            followers: req.user.id,
                            friends: req.user.id,
                        }
                    }, {
                        new: true,
                    })
                    .exec(function (err, user) {
                        if (err) return cb(err);
                        if (!user) return cb('user does not exist');
                        cb(null, myProfile, targetProfile);
                    })
            }
        },
        function (myProfile, targetProfile, cb) {
            if (blockStatus == false) {
                models.User.update({
                    _id: req.user.id,
                }, {
                        $pull: {
                            blockedUsers: req.params.id,
                        }
                    }, {
                        new: true,
                    })
                    .exec(function (err, user) {
                        if (err) return cb(err);
                        if (!user) return cb('user does not exist');
                        cb(null, myProfile, targetProfile);
                    })
            } else {
                models.User.update({
                    _id: req.user.id,
                }, {
                        $addToSet: {
                            blockedUsers: req.params.id,
                        }, $pull: {
                            following: req.params.id,
                            followers: req.params.id,
                            friends: req.params.id,
                            recentUsers: req.params.id,
                        }
                    },
                    {
                        new: true,
                    })
                    .exec(function (err, user) {
                        if (err) return cb(err);
                        if (!user) return cb('user does not exist');
                        cb(null, myProfile, targetProfile);
                    })
            }
        }

    ], function (err, myProfile, targetProfile) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        if (req.body.blockStatus) {
            return response.data({ "message": "user blocked." }, 200);
        } else {
            return response.data({ "message": "user unblocked." }, 200);
        }
    });
}


exports.accountSettings = function (req, res) {
    var isPrivate = false;
    var seen = false;
    async.waterfall([
        function (cb) {
            var privacyStatus;
            if (req.body.privacyStatus) {
                privacyStatus = true
            } else {
                privacyStatus = false
            }
            if (req.body.seen) {
                seen = true;
            } else {
                seen = false;
            }
            models.User.findOneAndUpdate({
                _id: req.user.id,
            }, {
                    $set: {
                        isPrivate: privacyStatus,
                        seen: seen,
                        language: req.body.language
                    }
                }, {
                    new: true,
                })
                .exec(function (err, user) {
                    if (err) return cb(err);
                    if (!user) return cb('user does not exist');
                    cb(null, user);
                })
        }
    ], function (err, user) {

        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data({ "isPrivate": user.isPrivate, "seen": user.seen, "language": user.language }, 200);
    });
}

exports.respondToFollowRequest = function (req, res) {
    async.waterfall([
        function (cb) {
            if (req.body.acceptedStatus == true || req.body.acceptedStatus == 'true' ||
                req.body.acceptedStatus == false || req.body.acceptedStatus == 'false') {
                cb(null);
            } else {
                return cb('Not a valid status');
            }
        },
        function (cb) {
            models.Notification.find({
                from: req.params.id,
                isRead: false
            })
                .exec(function (err, notifcation) {
                    if (err) return cb(err);
                    if (!notifcation) return cb('token profile does not exist');
                    var array = [];
                    async.eachSeries(notifcation, function (singleUser, next) {
                        if (singleUser) {
                            singleUser.isRead = true;
                            singleUser.save(function (err, result) {
                                if (err) return cb(err);
                                next()
                            })
                        }
                    }, function (err) {
                        if (err) return cb(err);
                        cb(null);
                    });
                })
        },
        function (cb) {
            models.User.findOne({
                _id: req.user.id
            })
                .exec(function (err, myProfile) {
                    if (err) return cb(err);
                    if (!myProfile) return cb('token profile does not exist');
                    cb(null, myProfile)
                })
        },
        function (myProfile, cb) {
            models.User.findOne({
                _id: req.params.id
            })
                .exec(function (err, targetProfile) {
                    if (err) return cb(err);
                    if (!targetProfile || !targetProfile.status) return cb('targetProfile does not exist');
                    cb(null, myProfile, targetProfile)
                });
        },
        function (myProfile, targetProfile, cb) {
            if (req.body.acceptedStatus == true || req.body.acceptedStatus == 'true') {
                cb(null, myProfile, targetProfile);
            } else {
                models.User.update({
                    _id: req.user.id
                },
                    {
                        $addToSet: {
                            rejectedRequests: req.params.id,
                        },
                    },
                    {
                        new: true
                    })
                    .exec(function (err, user) {
                        if (err) return cb(err);
                        models.User.update({
                            _id: req.user.id,
                        }, {
                                $pull: {
                                    pendingRequests: req.params.id,
                                }
                            }, {
                                new: true,
                            })
                            .exec(function (err, user) {
                                if (err) return cb(err);
                                if (!user) return cb('user does not exist');
                                cb(null, updatedMyUser, updatedTargetUser)
                            })
                        var response = response_helper(res);
                        return response.success("Request rejected.", 200);
                    })
            }
        },
        function (myProfile, targetProfile, cb) {
            var related = {
                isFollower: myProfile.addFollowersFlag(myProfile, req.params.id),
                isFollowing: myProfile.addFollowingFlag(myProfile, req.params.id)
            };

            if (related.isFollower) {
                return cb('target Id already a pursuer');
            } else {
                cb(null, related, myProfile, targetProfile)
            }
        },
        function (related, myProfile, targetProfile, cb) {
            if (related.isFollowing) {
                addFriend(myProfile, targetProfile, function (err, updatedMyUser) {
                    if (err) return cb(err);
                    addFriend(targetProfile, myProfile, function (err, updatedTargetUser) {
                        if (err) return cb(err);
                        cb(null, related, updatedMyUser, updatedTargetUser)
                    });
                });
            } else cb(null, related, myProfile, targetProfile)
        },
        function (related, myProfile, targetProfile, cb) {
            addFollower(targetProfile, myProfile, function (err, updatedMyUser) {
                if (err) return cb(err);
                addFollowing(targetProfile, myProfile, function (err, updatedTargetUser) {
                    if (err) return cb(err);
                    related.isFriend = myProfile.addFriendsFlag(myProfile, req.params.id)
                    cb(null, related, updatedMyUser, updatedTargetUser)
                });
            });
        },
        function (related, updatedMyUser, updatedTargetUser, cb) {
            models.User.update({
                _id: req.user.id,
            }, {
                    $pull: {
                        pendingRequests: req.params.id,
                    }
                }, {
                    new: true
                })
                .exec(function (err, user) {
                    if (err) return cb(err);
                    if (!user) return cb('user does not exist');
                    cb(null, related, updatedMyUser, updatedTargetUser)
                })
        },
        function (related, updatedMyUser, updatedTargetUser, cb) {
            var message, title, type;
            if (related.isFollower) {
                var messageFriends = 'Congratulations! You are now friends with ' + updatedMyUser.name;
                var titleFriends = "Friends";
                var typeFriends = "friends";
                message = 'Your request has been Accepted by' + updatedMyUser.name;
                title = "Pursue";
                type = "follow";

                var data = {};
                data.message = message;
                data.title = title;
                data.type = type;
                data.actionId = req.user.id;

                var dataFriends = {};
                dataFriends.message = messageFriends;
                dataFriends.title = titleFriends;
                dataFriends.type = typeFriends;
                dataFriends.actionId = req.user.id
                notification.create(req.params.id, req.user.id, null, typeFriends, titleFriends, messageFriends, (err, response) => {
                    if (err) {
                        id
                        return cb("Error in inserting notifcation logs.");
                    } else {
                        if (updatedTargetUser.device.deviceType == "android") {
                            androidPush.androidPushNotification(updatedTargetUser.device.deviceId, dataFriends, (err) => {

                            })
                        } else if (updatedTargetUser.device.deviceType == "ios") {
                            iosPush.apn(updatedTargetUser.device.deviceId, data, (err) => {

                            })
                        } else {
                            console.log("No valid device type.")
                        }
                        cb(null, related, updatedMyUser, updatedTargetUser);
                    }
                })
                notification.create(req.params.id, req.user.id, null, type, title, message, (err, response) => {
                    if (err) {
                        return cb("Error in inserting notifcation logs.");
                    } else {
                        if (updatedTargetUser.device.deviceType == "android") {
                            androidPush.androidPushNotification(updatedTargetUser.device.deviceId, data, (err) => {

                            })
                        } else if (updatedTargetUser.device.deviceType == "ios") {
                            iosPush.apn(updatedTargetUser.device.deviceId, data, (err) => {
                            })
                        } else {
                            console.log("No valid device type.")
                        }
                        cb(null, related, updatedMyUser, updatedTargetUser);
                    }
                })
            } else {
                message = 'Your request has been Accepted by ' + updatedMyUser.name;
                title = "Pursue";
                type = "follow";
                var data = {};
                data.message = message;
                data.title = title;
                data.type = type;
                data.actionId = req.user.id;
                notification.create(req.params.id, req.user.id, null, type, title, message, (err, response) => {
                    if (err) {
                        return cb("Error in inserting notifcation logs.");
                    } else {
                        if (updatedTargetUser.device.deviceType == "android") {
                            androidPush.androidPushNotification(updatedTargetUser.device.deviceId, data, (err) => {

                            })
                        } else if (updatedTargetUser.device.deviceType == "ios") {
                            iosPush.apn(updatedTargetUser.device.deviceId, data, (err) => {

                            })
                        } else {
                        }
                        cb(null, related, updatedMyUser, updatedTargetUser);
                    }
                })
            }
        },
    ], function (err) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.success("request accepted", 200);
    })
}

/*========================Upload Phone Directory==========================*/

exports.uploadPhoneDirectory = function (req, res) {
    const response = response_helper(res);
    if (!req.body.contacts) return response.failure('Please provide contacts', 400)
    let validContacts = []
    async.waterfall([
        (cb) => {
            async.eachSeries(req.body.contacts, function (item, next) {
                if (_.isEmpty(item.name) || _.isEmpty(item.phone)) {
                    next()
                } else {
                    validContacts.push(item)
                    next()
                }
            }, function (err) {
                if (err) return cb(err)
                cb(null)
            })
        },
        function (cb) {
            models.User.findOne({
                '_id': req.user.id
            })
                .exec(function (err, userData) {
                    if (err) cb(err)
                    cb(null, userData)
                })
        },
        function (userData, cb) {
            var dataToSave = [];
            async.eachSeries(validContacts, (requestedData, next) => {
                requestedData.userId = req.user.id;
                var desired = requestedData.phone.replace(/[^\w\s]/gi, '')
                var desired1 = desired.replace(/ /g, '');
                var trimedNumber;
                if (desired1.length < 9) {
                    trimedNumber = desired1
                } else {
                    trimedNumber = desired1.substr(desired1.length - 9);
                }
                requestedData.phone = trimedNumber;
                dataToSave.push(requestedData);
                models.User.findOne({
                    'countryCodeAndNumber': {
                        $regex: trimedNumber
                    }
                })
                    .exec(function (err, data) {
                        if (err) cb(err);

                        if (data != null) {
                            requestedData.isRequested = userData.addFollowRequestFlag(userData._id, data);
                            requestedData.isBlocked = userData.addBlockedUserFlag(userData, data._id);
                            requestedData.isFriend = userData.addFriendsFlag(userData, data._id);
                            requestedData.isFollowing = userData.addFollowingFlag(userData, data._id);
                            requestedData.isFollower = userData.addFollowersFlag(userData, data._id);
                            requestedData.id = data._id;
                            requestedData.isNumberExist = true;
                        } else {
                            requestedData.isRequested = false;
                            requestedData.isBlocked = false;
                            requestedData.isFriend = false;
                            requestedData.isFollowing = false;
                            requestedData.isFollower = false;
                            requestedData.id = false;
                            requestedData.isNumberExist = false;
                        }
                        next();
                    })

            }, function (err) {
                cb(null, validContacts, dataToSave)
            });
        },
        function (requestedData, dataToSave, cb) {
            models.PhoneDirectory.find()
                .where('userId').equals(req.user.id)
                .exec((err, data) => {
                    if (err) cb(err)
                    var formatData = data.map(value => {
                        let format = {}
                        format.name = value.name;
                        format.phone = value.phone;
                        return format
                    })
                    cb(null, formatData, requestedData, dataToSave)
                })
        },
        function (userContacts, dirReqData, dirNewData, cb) {
            var formatData = validContacts.map(value => {
                let format = {}
                format.name = value.name;
                format.phone = value.phone
                return format
            })

            var dif = _.differenceWith(formatData, userContacts, _.isEqual);

            if (dif.length == 0) return cb(null, dirReqData)

            var insertContacts = dif.map(value => {
                let format = {}
                format.name = value.name;
                format.phone = value.phone;
                format.userId = req.user.id;
                return format
            })

            models.PhoneDirectory.insertMany(insertContacts, (err, savedData) => {
                if (err) cb(err)
                cb(null, dirReqData)
            })
        }
    ], function (err, status) {

        if (err) return response.failure(err, 400);
        models.User.findOne({
            '_id': req.user.id
        })
            .exec(function (err, userData) {
                if (err) return response.failure(err, 400);
                var arrays = [];
                status.forEach(function (item) {
                    if (item.phone != userData.phoneNumber.phone && item.isFollowing == false && item.isRequested == false && item.isBlocked == false) {
                        arrays.push(item)
                    }
                })
                return res.status(200).send({
                    'is_success': true,
                    'responseCode': 200,
                    'data': arrays,
                    'message': 'phone directory uploaded'
                });
            });
    })
}

//===============================LogOut====================================//

exports.logOutUser = function (req, res) {
    var response = response_helper(res);
    models.User.findOne({ _id: req.user.id })
        .exec(function (err, result) {
            if (err) return response.failure(err, 400);
            if (!result) return response.failure(400, 'user not found');
            result.token = "",
                result.device.deviceId = "",
                result.save(function (err, user) {
                    if (err) return response.failure(err, 400);
                    if (!user) return response.failure(400, 'user not found');
                    response.success("Logout", 200);
                })
        })
}

//===============================LogOut====================================//

//#####################################  Get promotions with Lat Long  ########################################


exports.userPromotions = function (req, res) {
    var response = response_helper(res);
    var paging = {
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 10,
    }

    async.waterfall([
        function (cb) {
            models.Promotion.find()
                .exec(function (err, promotions) {
                    if (err) return cb(err);
                    cb(null, promotions);
                })
        },

        function (promotions, cb) {
            var promoArray = [];
            var query;
            _.each(promotions, function (promotion) {
                query = {
                    promoLocation: {
                        $nearSphere: {
                            $geometry: {
                                type: "Point",
                                coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.latitude)]
                            },
                            $maxDistance: req.body.radius * 1000
                        }
                    }
                };

                models.Promotion.find(query)
                    // .skip(paging.skip)
                    // .limit(paging.limit)
   
                    .exec(function (err, result) {
                        if (err) return cb(err);
                        var obj = {};
                        _.each(result, function (r) {
                            obj.video = r.video;
                            obj.image = r.image;
                            obj.promotionText;
                            promoArray.push(obj)
                        });
                    })
            });
            cb(null, promoArray);
        }
    ], function (err, user) {
        console.log(user, 'testing');
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data({ 'user': user }, 200);
    });
}
/*******************************************************************************
Api to change  password.
*******************************************************************************/
exports.changePassword = function (req, res) {
    async.waterfall([
         function (cb) {
            user_validate.change_pass(req.body).is_valid(function (err, form) {
                if (err) return cb(err);
                cb(null, form.data);
            });
        },
        function (form, cb) {
            if(form.password.trim()) {
                  cb(null, form);
            } else {
                  return cb('Empty string is not allowed.');
            }
        }, 
        function (form, cb) {
            models.User.findOne({ _id: req.user.id })
                .exec(function (err, result) {
                    if (err) return cb(err);
                    if (!result) return cb('user does not exist');
                    if (result.validPassword(form.oldPassword)) {
                        var encryptedPass = models.User.schema.methods.generateHash(form.password)
                        result.password = encryptedPass,
                        result.save(function (err, result) {
                            cb(null, result)
                        })
                    } else {
                        return cb('Oops! old password does not match.');
                    }
                })
        },
    ], function (err, result) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.success('password is changed successfully.', 200);
    })
}

/*******************************************************************************
Api for forgot password
*******************************************************************************/

exports.forgotPassword = function (req, res) {
    var response = response_helper(res);
    async.waterfall([
        function (cb) {
            user_validate.resetPassword(req.body).is_valid(function (err, form) {
                if (err) return cb(err);
                cb(null, form.data);
            });
        },
        function (form, cb) {
            models.User.findOne({"phoneNumber.countryCode": form.countryCode, "phoneNumber.phone": form.phone})
                .exec(function (err, result) {
                    if (err) return cb(err);
                    if (!result) return cb('user does not exist');
                    result.temporaryToken = authentication.getToken(result);
                    result.save(function (err, data) {
                        if (err) return cb(err);
                        if (!data) return cb('user does not exist');
                        cb(null, data)
                    });
                })
        },
    ], function (err, resultedData) {
        console.log(resultedData, 'testing14645234');
        if (err) return response.failure(err, 400);
        var user = {};
        user.temporaryToken = resultedData.temporaryToken;
        user._id = resultedData.temporaryToken;
        return response.data(user, 200);
    })
}

/*******************************************************************************
Api to reset  password.
*******************************************************************************/

exports.resetPassword = function (req, res) {
    var response = responseHelper(res);
    var manValues = [req.body.resetPasswordToken, req.body.password]
    async.waterfall([
        function (cb) {
            user_validate.resetPassword(req.body).is_valid(function (err, form) {
                if (err) return cb(err);
                cb(null, form.data);
            });
        },
        function (form, cb) {
            if (form.password.trim()) {
                cb(null, form);
            } else {
                return cb('Empty string is not allowed.');
            }
        },
        function (form, cb) {
            models.User.findOne({ countryCode: form.countryCode, phone: form.phone })
                .exec(function (err, result) {
                    if (err) return cb(err);
                    if (!result) return cb('Invalid token');
                    var encryptedPass = models.User.schema.methods.generateHash(req.body.password);
                    result.password = encryptedPass;
                    result.temporaryToken = '';
                    result.token = authentication.getToken(result);
                    result.save(function (err, savedPassWord) {
                        cb(null, savedPassWord)
                    })
                })
        },
    ], function (err, resultedData) {
        var response = responseHelper(res);
        if (err) return response.failure(err, 400);
        return response.data(resultedData, 200);
    })
}

exports.tokenVerification = function (req, res) {
    async.waterfall([
        function (cb) {
            user_validate.userYerification(req.body).is_valid(function (err, form) {
                if (err) return cb(err);
                cb(null, form.data);
            });
        },
        function (form, cb) {
            models.User.findOne({ temporaryToken: form.temporaryToken })
                .exec(function (err, result) {
                    if (err) return cb(err);
                    if (!result) return cb('user does not exist');
                    result.token = authentication.getToken(result);
                    result.temporaryToken = '';
                    result.isRegistration = true;
                    result.firebaseId = form.firebaseId;
                    result.save(function (err, user) {
                        if (err) return cb(err);
                        if (!user) return cb('user does not exist');
                        cb(null, user)
                    })
                })

        },
    ], function (err,  user) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(user, 200);
    })
}

/*******************************************************************************
Api for login.
*******************************************************************************/
exports.userLogin = function (req, res) {
    async.waterfall([
        function (cb) {
            user_validate.login_form(req.body).is_valid(function (err, form) {
                if (err) return cb(err);
                cb(null, form.data);
            });
        },
        function (form, cb) {
            if (form.deviceType == 'android' || form.deviceType == 'web' || form.deviceType == 'ios') {
                cb(null, form);
            } else {
                return cb("Device Type can be android, web or ios");
            }
        },
        function (form, cb) {
            models.User.findOne({
                email: form.email
            }, function (err, user) {
                if (err) return cb(err);
                if (!user) return cb('user does not exist');
                if (user.validPassword(form.password)) {
                    cb(null, form, user);
                } else {
                    return cb('Oops! Wrong password.')
                }
            })
        },
        function (form, user, cb) {
            models.Version.findOne()
                .select('normalAndroidVersion criticalAndroidVersion normalIOSVersion criticalIOSVersion')
                .exec(function (err, version) {
                    if (err) return cb(err);
                    if (!version) return cb('versions not found in DB')
                    form.deviceType = form.deviceType;
                    form.deviceId = form.deviceId;
                    if (form.deviceType == 'android') {
                        form.androidVersion = form.version;
                    } else if (form.deviceType == 'ios') {
                        form.iosVersion = form.version;
                    }
                    checkVersion(form, version, function (versionResult) {
                        console.log(versionResult);
                        if (versionResult.isCritical)
                            return cb(versionResult);
                        cb(null, user, versionResult)
                    })
                });
        },
        function (user, versionResult, cb) {
            user.token = authentication.getToken(user);
            user.save(function (err, result) {
                if (err) return cb(err);
                cb(null, versionResult, result);
            })
        }
    ], function (err, versionResult, user) {
        var response = response_helper(res);
        var data = {
            versionResult: versionResult || err,
            user: user
        };
        if (err && !err.isCritical) return response.failure(err, 400);
        return response.data(data, 200);
    })
}