"use strict";
const async = require('async');
const models = require('mongoose').models;
const response_helper = require('../helpers/response');
const mongoose = require('mongoose');
const _ = require('lodash');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
var config = require('../settings/config');

//==========================================Start createPromotion============================================//

exports.create = (req, res) => {
    var response = response_helper(res);
    var wordModel = {};
    wordModel.actual_word = req.body.actual_word;
    wordModel.manipulate_word = req.body.manipulate_word;
    wordModel.createdBy = req.user.id;
    wordModel.isBlocked = false;
    wordModel.status = false;
    var word = new models.Word(wordModel);
    word.save(function (err, result) {
        if (err) {
            return response.failure(err, 400);
        } else {
            return response.data(result, 200);

        }
    });
}
//==========================================End createPromotion============================================//
//========================================= All Promotions ================================================//
exports.all = function (req, res) {
    var query = {
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 10
    }
     var response = response_helper(res);
     models.Word.find()
        .skip(query.skip > 0 ? ( ( query.skip - 1 ) * query.limit ) : 0)
        .limit(query.limit)
        .exec(function (err, words) {
            if (err) {
                return response.failure(err, 400);
            } else if (!words) {
                return response.failure(err, 400, 'Word is  not found.');
            } else {
                models.Word.find()
                .exec(function (err, wordcount) {
                    if (err) {
                        return response.failure(err, 400);
                    } else if (!wordcount) {
                        return response.failure(err, 400, 'Word is  not found.');
                    } else {
                    var wordAndCount = {words:words, count:wordcount.length}   
                    return response.data(wordAndCount, 200);
                    }
                });

           }
    });
};

exports.getAbusedWord = function (req, res) {
     var response = response_helper(res);
     var stringValue = req.body.stringValue;
     var strInArray = stringValue.split(/(\s+)/);
     strInArray = strInArray.join().split(/\s|,/).filter(Boolean);
     models.Word.find({ actual_word: { $in: strInArray}}, {actual_word:1, manipulate_word:1})
        .exec(function (err, word) {
            var replace;
            if (err) {
                return response.failure(err, 400);
            } else if (!word) {
                return response.failure(err, 400, 'No String is found.');
            } else {
                if(word) {
                   for (var i = 0; i < word.length; i++) {
                       stringValue = stringValue.replace(new RegExp(word[i].actual_word,"g"), word[i].manipulate_word);
                   }
                   return response.data({stringValue}, 200);
                } else {
                   return response.data({stringValue}, 200);
                }
                
            }
           
    });
};

exports.get = function (req, res) {
     var response = response_helper(res);
     if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        return response.failure('Not a valid _id', 400, 'id is invalid')
     }
     models.Word.findOne({_id: req.params.id})
        .exec(function (err, word) {
            if (err) {
                return response.failure(err, 400);
            } else if (!word) {
                return response.failure(err, 400, 'Promotion is  not found.');
            } else {
                return response.data(word, 200);
           }
    });
};

//========================================= End Promotion ==================================================
exports.blockAndUnblockWord = function (req, res) {
    async.waterfall([
        function (cb) {
            models.Word.findOne({
                _id: req.body.id
            })
            .exec(function (err, word) {
                if (err) return cb(err);
                if (!word) return cb('Word does not exist');
                cb(null, word);
            });
        },
        function (word, cb) {
            if(req.body.isBlocked == 'true'){
               word.isBlocked = true;
            } else {
               word.isBlocked = false; 
            }
            word.save(function (err, updatedWord) {
                if (err) return cb(updatedWord);
                cb(null, updatedWord);
            });
        },
       
    ], function (err, word) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(word, 200);
    })
};

exports.change_status = function (req, res) {
    async.waterfall([
        function (cb) {
            models.Word.findOne({
                _id: req.body.id
            })
                .exec(function (err, word) {
                    if (err) return cb(err);
                    if (!word) return cb('Word does not exist');
                    cb(null, word);
                });
        },
        function (word, cb) {
            if(req.body.status == 'true'){
               word.status = true;
            } else {
               word.status = false; 
            }
            word.save(function (err, updatedWord) {
                if (err) return cb(updatedWord);
                cb(null, updatedWord);
            });
        },
       
    ], function (err, word) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(word, 200);
    })
};


//==========================================Start deletePromotion============================================//
exports.delete = function (req, res) {
    var response = response_helper(res);
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        return response.failure('Not a valid _id', 400, 'id is invalid')
    }
    models.Word.findOne({ _id: req.params.id }, function (err, word) {
        if (err) {
            return response.failure(err, 400);
        } else if (!word) {
            return response.failure(err, 400, 'Word record is not found');
        } else {
            word.remove(function (err, done) {
                if (err) {
                    return response.failure(err, 400);
                } else {
                    response.success('Word record is  deleted successfully.', 200);
                }
            })
        }
    });
};

//==========================================End deletePromotion============================================//



//==========================================Start updatePromotion============================================//

exports.update = function (req, res) {
    var response = response_helper(res);
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        return response.failure('Not a valid _id', 400, 'id is invalid')
    }
    models.Word.findOne({ _id: req.params.id }, function (err, word) {
        if (err) {
            return response.failure(err, 400);
        } else if (!word) {
            return response.failure(err, 400, 'Word is  not found.');
        } else {
            word.actual_word = req.body.actual_word;
            word.manipulate_word = req.body.manipulate_word;
            word.save(function (err, result) {
                if (err) {
                    return response.failure(err, 400);
                } else {
                    return response.data(word, 200);
                }
            })
        }
    });  
}


//==========================================End updatePromotion============================================//
exports.word_search = function(req, res) {
    models.Word.find({actual_word: new RegExp(req.query.searchText, 'i') })
        .sort('created_at')
        .exec(function(err, words) {
            var response = response_helper(res);
            if (err) return response.failure(err, 400);
            else if (!words) return response.failure(err, 400, 'No word found')
            else return response.data(words, 200);
    })
};