"use strict";

const models = require('mongoose').models;
const jwt = require('jsonwebtoken');
const async = require('async');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const FCM = require('fcm-node');
const config = require('../settings/config');
const response_helper = require('../helpers/response');
const authentication = require('../middleware/authentication');
const userLoginAndRegistration = require('../components/authorization')
const commentForm = require('../validators/comment')
const notification = require('../components/notificationLogs')
const androidPush = require('../components/androidNotification');
const iosPush = require('../components/iosNotification');

exports.create = function (req, res) {

    /*================need notification ======================*/

    async.waterfall([
        function (cb) {
            commentForm.commentCreate(req.body).is_valid(function (err, form) {
                if (err) return cb(err);
                cb(null, form.data);
            });
        },
        function (form, cb) {
            form.createdBy = req.user.id;
            var comment = new models.Comment(form);
            comment.save(function (err, comment) {
                if (err) return cb(err);
                cb(null, form, comment);
            });
        },
        function (form, comment, cb) {
            models.Post.findOneAndUpdate({
                _id: req.params.id
            }, {
                    $addToSet: { comments: comment.id }
                }, {
                    new: true
                })
                .exec(function (err, post) {
                    if (err) return cb(err);
                    if (!post) return cb('post does not exist');
                    models.Comment.findOne({_id:comment.id})
                        .populate({
                            path: "createdBy",
                            select: "name profilePic"
                        })
                        .exec(function (err, datas) {
                            if (err) return cb(err);
                            if (!datas) return cb('post does not exist');
                            cb(null, datas, post);
                        })
                })
        },
        function (data, post, cb) {
            models.User.find({_id: req.user.id})
            .select('name')
            .exec(function (err, user) {
                if (err) return cb(err);
                cb(null, data, post, user[0]);
            })
        },
        function (data, post, user, cb) {
            models.User.find({_id: post.createdBy})
            .exec(function (err, postUser) {
                if (err) return cb(err);
                cb(null, data, post, user, postUser);
            })
        },
        function (data, post, user, postUser, cb) {
            var message = user.name + ' has commented on you post: "' + req.body.text + '."';
            var type = "commentOnPost";
            var title = "Comment"
            if (post.createdBy != req.user.id) {
                var device = postUser[0].device;
                var notificationData = {};
                notificationData.message = message;
                notificationData.title = title;
                notificationData.type = type;
                notificationData.actionId = post.id;
                if (device.deviceType == "android") {
                    androidPush.androidPushNotification(device.deviceId, notificationData, (err) => {
                    
                    })
                } else if (device.deviceType == "ios") {
                    iosPush.apn(device.deviceId, notificationData, (err) => {

                    })
                } else {
                    console.log("No valid device type.")
                }
                notification.create(post.createdBy, req.user.id, post.id, type, title, message, (err, response) => {
                    if (err) {
                        return cb("Error in creating comment notifications.");
                    } else {
                        cb(null, data);
                    }
                })
            } else {
                cb(null, data);
            }
        }
    ], function (err, comment) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(comment, 200);
    })
}



exports.all = function (req, res) {
    var query = {
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 10
    }
    async.waterfall([
        function (cb) {
            models.Post.find()
                .skip(query.skip)
                .limit(query.limit)
                .exec(function (err, posts) {
                    if (err) return cb(err);
                    cb(null, posts);
                })
        },
        function (posts, cb) {
            async.eachSeries(posts, function (singlePost, next) {
                models.Post.findOneAndUpdate(
                    { _id: singlePost._id },
                    {
                        $addToSet: { views: req.user.id },
                        viewCount: singlePost.viewCount + 1
                    },
                    { new: true },
                    function (err, post) {
                        if (err) return cb(err);
                        if (!post) return cb('views updating-- post does not exist');
                        singlePost._doc = post._doc;
                        singlePost._doc.isLiked = singlePost.addlike(singlePost, req.user.id);
                        singlePost._doc.likesCount = singlePost.likeCount(singlePost);

                        next()
                    }
                );
            }, function (err) {
                if (err) return cb(err);
                cb(null, posts)
            });
        },
        function (posts, cb) {
            models.Post.count()
                .exec(function (err, count) {
                    cb(null, posts, count);
                });
        }
    ], function (err, posts, count) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.page(posts, 200, count, query.skip);
    })
}


exports.update = function (req, res) {
    async.waterfall([
        function (cb) {
            models.Post.findOne({
                _id: req.params.id
            })
                .exec(function (err, post) {
                    if (err) return cb(err);
                    if (!post) return cb('post does not exist');
                    cb(null, post);
                })
        },
        function (post, cb) {
            post.title = req.body.title || post.title;
            post.description = req.body.description || post.description;
            post.images = req.body.images || post.images;
            post.save(function (err, updatedPost) {
                if (err) return cb(updatedPost);
                cb(null, updatedPost);
            });
        }
    ], function (err, post) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(post, 200);
    })
};



exports.like = function (req, res) {
    async.waterfall([
        function (cb) {
            models.Post.findOneAndUpdate({
                _id: req.params.id
            }, {
                    $addToSet: {
                        likes: req.user.id
                    }
                }, {
                    new: true
                }, function (err, post) {
                    if (err) return cb(err);
                    if (!post) return cb('post does not exist');
                    cb(null, post);
                })
            /*==============notification of like ==================*/
        },
        function (post, cb) {
            post._doc.isLiked = post.addlike(post, req.user.id);
            post._doc.likesCount = post.likeCount(post);
            cb(null, post._doc);
        }
    ], function (err, post) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(post, 200);
    })
}


exports.dislike = function (req, res) {
    /*==============notification of like ==================*/
    async.waterfall([
        function (cb) {
            models.Post.findOneAndUpdate({
                _id: req.params.id
            }, {
                    $pull: {
                        likes: req.user.id
                    }
                }, {
                    new: true
                }, function (err, post) {
                    if (err) return cb(err);
                    if (!post) return cb('post does not exist');
                    cb(null, post);
                })

        },
        function (post, cb) {
            post._doc.isLiked = post.addlike(post, req.user.id);
            post._doc.likesCount = post.likeCount(post);
            cb(null, post._doc);
        }
    ], function (err, post) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data(post, 200);
    })
}