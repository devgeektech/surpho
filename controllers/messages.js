"use strict";

const models = require('mongoose').models;
const jwt = require('jsonwebtoken');
const async = require('async');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const FCM = require('fcm-node');
const config = require('../settings/config');
const response_helper = require('../helpers/response');
const authentication = require('../middleware/authentication');
const userLoginAndRegistration = require('../components/authorization')
const social_validate = require('../validators/user')

exports.getList = function(req, res) {
     async.waterfall([
        function(cb) {
            if(!req.params.id) {
                return cb('selected userId is required')
            } else cb(null);
        },
        function(cb) {
            models.ChatRoom.findOne({})
                .exec(function (err, version) {
                    if (err) return cb(err);

                    checkVersion(form, version, function (versionResult) {
                        if (versionResult.isCritical) {
                            return cb(versionResult);
                        } else cb(null, versionResult, form)
                    })

                });

        },
        function (versionResult, form, cb) {
            var email = form.socialId + '@' + form.loginType + '.com'; 
            
            models.User.findOne({
                email: email
            })

                .exec(function(err, user) {
                    if(err) return cb(err);
                    if (!user) {
                        firstTimeLogin = true;
                        var user = new models.User();
                        user.email = email;
                        user.social_media_links[form.loginType].id = form.socialId;
                        user.name = form.name;
                    };
                    user.profilePic = form.profilePic;
                    user.device.deviceId = form.deviceId;
                    user.device.deviceType = form.deviceType;
                    user.token = authentication.getToken(user);
                    user.save(function (err, updatedUser) {
                        if (err) return cb(err);
                        
                        cb(null, versionResult, firstTimeLogin, userSignUpReturnModel(user));
                    })

                })
        }
    ], function (err, versionResult, firstTimeLogin, user) {
        var response = response_helper(res);
        var data = {
            firstTimeLogin: firstTimeLogin,
            versionResult: versionResult || err,
            user: user
        };
        if (err && !err.isCritical) return response.failure(err, 400);
        return response.data(data, 200);
    })
}