var express = require('express');
var mongoose = require('mongoose');
var http = require('http');
var config = require('./settings/config');
const models = require('mongoose').models;
const jwt = require('jsonwebtoken');
var io = require('socket.io')(http);
const cors = require('cors');

var app = express();
var router = express.Router()
app.use(cors({ origin: '*', optionsSuccessStatus: 200 }));
router.use(function (req, res, next) {
    next()
  })
// app.use(function (req, res, next) {
//     console.log('Request URL:', req.originalUrl);
//     next()
// })

app.use('/uploads', express.static(__dirname + '/uploads'));

var server = http.createServer(app);

io = require('socket.io').listen(server);

require('./settings/socket').configure(io);

require('./settings/database').configure(mongoose);
require('./settings/express').configure(app);
require('./settings/routes').configure(app);



var port = process.env.PORT || config.web_server.port || 3000;
server.listen(port, function() {
    
    console.log('express running at: ' + port);
});


exports.module = exports = app; 