var forms = require('forms');
var fields = forms.fields;
var validators = forms.validators;
var handler = require('../helpers/input_handler');

var social_form = forms.create({
    socialId: fields.string({
        required: validators.required('socialId is required')
    }),
    deviceType: fields.string({
        required: validators.required('deviceType is required')
    }),
    deviceId: fields.string({
        required: validators.required('deviceId is required')
    }),
    loginType: fields.string({
        required: validators.required('loginType is required')
    }),
    androidVersion: fields.string(),
    iosVersion: fields.string(),
    name: fields.string(),
    profilePic: fields.string(),
});

var signUp_form = forms.create({
    userName: fields.string({
        required: validators.required('username is required')
    }),
    email: fields.string({
        required: validators.required('email is required')
    }),
    deviceType: fields.string({
        required: validators.required('deviceId is required')
    }),
    countryCode: fields.string({
        required: validators.required('country code is required')
    }),
    phone: fields.string({
        required: validators.required('phone is required')
    }),
    deviceId: fields.string({
        required: validators.required('Device Id is required')
    }),
    password: fields.string({
        required: validators.required('password is required')
    }),
    confirmPassword: fields.string({
        required: validators.required('confirm password is required')
    }),
    version: fields.string(),
    description:fields.string(),
    profilePic: fields.string(),
    name: fields.string(),
});
var login_form = forms.create({
    email: fields.string({
        required: validators.required('email is required')
    }),
    deviceType: fields.string({
        required: validators.required('deviceId is required')
    }),
    deviceId: fields.string({
        required: validators.required('Device Id is required')
    }),
    password: fields.string({
        required: validators.required('password is required')
    }),
    version: fields.string(),
   
});


var userUpdate = forms.create({
    description: fields.string()
})
var resetPassword = forms.create({
    countryCode: fields.string({
        required: validators.required('country code is required')
    }),
    phone: fields.string({
        required: validators.required('phone is required')
    }),
})

var userYerification = forms.create({
    temporaryToken: fields.string({
        required: validators.required('token  is required')
    }),
    firebaseId: fields.string({
        required: validators.required('firebase id is required')
    }),
})
var change_pass = forms.create({
    oldPassword: fields.string({
        required: validators.required('old password  is required')
    }),
    password: fields.string({
        required: validators.required('new password is required')
    }),
})

module.exports.social_form = handler.validations(social_form);
module.exports.userUpdate = handler.validations(userUpdate);
module.exports.signUp_form = handler.validations(signUp_form);
module.exports.resetPassword = handler.validations(resetPassword);
module.exports.userYerification = handler.validations(userYerification);
module.exports.change_pass = handler.validations(change_pass);
module.exports.login_form = handler.validations(login_form);



