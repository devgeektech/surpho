var forms = require('forms');
var fields = forms.fields;
var validators = forms.validators;
var handler = require('../helpers/input_handler');

var post_form = forms.create({
    media: fields.array({
        required: validators.required('media is required')
    }),
    tags: fields.array(),
    title: fields.string(),
});


module.exports.post_form = handler.validations(post_form);

