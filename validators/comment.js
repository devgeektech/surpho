var forms = require('forms');
var fields = forms.fields;
var validators = forms.validators;
var handler = require('../helpers/input_handler');

var commentCreate = forms.create({
    text: fields.string({
        required: validators.required('text is required')
    })
});


module.exports.commentCreate = handler.validations(commentCreate);

