"use strict";

const jwt = require('jsonwebtoken');
const models = require('mongoose').models;
const config = require('../settings/config');
const validator = require('validator');
const async = require('async');

exports.requiresToken = function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (!token) {
        return res.status(403).send({
            responseCode: 403,
            success: false,
            message: 'Token is required.'
        });
    }
    models.User.findOne({
        token: token
    }, function(err, user) {
        if(err) return res.status(403).send({
            responseCode: 403,
            message: 'err in user finding via token',
            data: {}
        });
        if(!user) return res.status(403).send({
            responseCode: 403,
            message: 'token expired',
            data: {}
        });

        jwt.verify(token, config.auth.secret, {
            ignoreExpiration: true
        }, function(err, claims) {
            if (err) {
                return res.json({ responseCode: 403, success: false, message: 'Failed to authenticate token.' });
            } else {
                req.user = claims;
                next();
            }
        });

    })
    
};

exports.getToken = function(user) {
    var claims = {
        id: user._doc._id,
        email: user._doc.email,
        role: user._doc.role,
        deviceId: user.device.deviceId || '',
        deviceType: user.device.deviceType || ''
    }
    return jwt.sign(claims, config.auth.secret, { expiresIn: '1h' });
};

exports.requireLogin = function(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/login');
};