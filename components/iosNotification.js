var apn = require('apn');
var options = {
    token: {
        key: "config/sslcerts/AuthKey_FTW38NV746.p8",
        keyId: "FTW38NV746",
        teamId: "T75HV43R5K"
    },
    production: true
};

exports.apn = function(deviceId, contentData, callback) {
    console.log("contant Wala data",contentData);
    var apnProvider = new apn.Provider(options);
    //let deviceToken = "8fb90448a26898e203aef7f66eaf49532a7024ce021225c1ae74e0c6dc3eb5df"
    var deviceToken = deviceId;

    var note = new apn.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.badge = 0;
    // note.type = contentData.type;
    note.sound = "ping.aiff";
    note.alert = { body :contentData.message,
                title : contentData.title,
                type: contentData.type,
                actionId: contentData.actionId,
    };
    // note.payload = { 'messageFrom': contentData.message };
    note.topic = "com.surfho";
   
    apnProvider.send(note, deviceToken).then((result) => {
        
    console.log('ios notification', JSON.stringify(result))
    });
}