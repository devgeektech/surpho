"use strict";

const FCM = require('fcm-node');
const config = require('../settings/config');
const mongoose = require('mongoose');
const models = require('mongoose').models;

exports.androidPushNotification = (deviceId, contentDetail, callback) => {
    var fcm = new FCM(config.push_notification.serverKey);
    var message;

    if (deviceId instanceof Array && deviceId.length > 0) {
        message = {
            registration_ids: deviceId,
            notification: {
                title: contentDetail.title,
                body: contentDetail.message
            },
            data: contentDetail
        }
    } else {
        //deviceId = "fIcIN0CRBjs:APA91bFVdYGZHlJfQc81HYxxF9yu0svEGqK0qa2PH8eJi2gim7yXcLBR2prVYxHlHzVF96FDJKmI1QwwXX6vtpiAlqi8oxhTvi9cht2JeZSbQVYpdpopaK9xAhn-a108N6kvujLCaZZA";
        message = {
            to: deviceId,
            notification: {
                title: contentDetail.title,
                body: contentDetail.message
            },
            data: contentDetail
        }
    }

    fcm.send(message, function(err, response) {
        if (err) {
            return callback({ error: err });
        } else {
            console.log("Successfully sent with response: ", response);
        }
    });
}