"use strict";

const models = require('mongoose').models;
const jwt = require('jsonwebtoken');
const async = require('async');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const FCM = require('fcm-node');
const config = require('../settings/config');
const response_helper = require('../helpers/response');
const authentication = require('../middleware/authentication');
const userLoginAndRegistration = require('../components/authorization')
const user_validate = require('../validators/user');
var FifoArray = require('fifo-array');



exports.create = function(to, from, post, type, title, description, callback) {
    var notification = models.Notification()
    var notification = new models.Notification({
        to: to,
        from: from,
        post: post,
        type: type,
        title: title,
        description: description
    });
    notification.save(function(err, notification) {
        if (err) return callback(err);
        console.log(notification);
        callback(null, notification);
    });
}


/* 
 * GET NOTIFICATIONS
 */ 

exports.getAllNotifications = function (req, res) {
    var query = {
        limit: req.query.limit || 10,
        skip: req.query.skip || 0
    }
    async.waterfall([
        function (cb) {
            models.Notification.find({
                to: req.user.id
            })
            
            .skip(query.skip)
            .limit(query.limit)
            .sort([['createdAt', -1]])
            .populate({
                path: "from",
                select: "name userName profilePic"
            })
            .where('status').equals(true)
            .exec(function (err, notification) {
                if (err) return cb(err);
                if (notification.length <= 0) return cb('No Notifications found.');
                cb(null, notification);
            })
        },
        function (notification, cb) {
            models.Notification.count({to: req.user.id})
                .skip(query.skip)
                .limit(query.limit)
                // .where('status').equals(true)
                .exec(function (err, count) {
                    console.log(count)
                    cb(null, notification, count);
                });
        }
    ], function (err, notification, count) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.page(notification, 200, count, query.skip);
    })
}


exports.changeStatus = function (req, res) {
    async.waterfall([
        function (cb) {
            models.Notification.findOneAndUpdate({
                _id: req.params.id
            }, {
                $set: {
                    status: false
                }   
                }, function (err, notification) {
                    if (err) return cb(err);
                    if (!notification) return cb('notification not exist.');
                    cb(null, notification);
                })
            }
    ], function (err, notification) {
        var response = response_helper(res);
        if (err) return response.failure(err, 400);
        return response.data({ "message": "notification status changed." }, 200);
    });
}