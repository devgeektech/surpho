"use strict";

const jwt = require('jsonwebtoken');
const models = require('mongoose').models;
const config = require('../settings/config');
const validator = require('validator');
const async = require('async');
const authentication = require('../middleware/authentication');
const mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
exports.userRegistration = (email, username, password, cb) => {
    async.waterfall([
        function(cb) {
            if (email == undefined) {
                return cb('Email  is required.');
            }
            cb(null);
        },
        function(cb) {
            if (password == undefined) {
                return cb('Password is required.');
            }
            cb(null);
        },
        function(cb) {
            var emailValid = validator.isEmail(email)
            if (!emailValid) {
                return cb('Email address not valid.');
            }
            cb(null);
        },
        function(cb) {
            var user = new models.User({
                password: models.User.schema.methods.generateHash(password),
                email: email,
                username: username
            });
            user.save(function(err, newUser) {
                if (err) return cb(err);
                cb(null, newUser);
            });

        }
    ], 
    function(err, newUser) {
        if (err) {
            callback(err);
        } else {
            callback(null, newUser);
        }
    });
}

 
exports.login = (userObject, callback) => {
    async.waterfall([
            function(cb) {
                if (userObject.email == undefined || userObject.password == undefined) {
                    return cb('Mandatory field missing.');
                }
                cb(null);
            },
            function(cb) {
                var emailValid = validator.isEmail(userObject.email)
                if (!emailValid) {
                    return cb('Email address not valid.');
                }
                cb(null);
            },
            function(cb) {
                models.User.findOne({ email: userObject.email, password: userObject.password }, function(err, user) {
                    if (err) {
                        if (err) return cb(err);
                    } else {
                        if (!user) {
                            return cb('Oops! Wrong email and password.')
                        } else {
                           cb(null, user);
                        }
                    }
                })
            },
             function(user, cb) {
                user.token = authentication.getToken(user);
                user.lastLogin = Date.now();
                user.save(function(err, userUpdated) {
                    if (err) return cb(err);
                    cb(null, userUpdated);
                })
            }
        ],
        function(err, user) {
            if (err) {
                return callback(err);
            }
            callback(null, user);
        })
}

//logout

exports.logout = function(id, cb) {
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return cb({ err: 'Id is invalid' })
    }
    models.Users.findOne({ _id: id }, function(err, user) {
        if (!user) {
            return cb({ err: 'No Such User.' })
        }
        user.token = "";
        user.save(function(err, updatedUser) {
            if (err) return cb({ err: err })
            cb({ success: 'User Is Logged Out.' });
        })
    })
}